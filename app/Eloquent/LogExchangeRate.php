<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogExchangeRate extends Model
{
    public $timestamps = false;
    protected $table = 'log_exchange_rate';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }
}
