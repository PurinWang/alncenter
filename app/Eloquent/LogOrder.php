<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogOrder extends Model
{
    public $timestamps = false;
    protected $table = 'log_order';

    /*
     *
     */
    public function __construct ()
    {
    }

}
