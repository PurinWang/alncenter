<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivity extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
