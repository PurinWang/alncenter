<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCoin extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coin';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
