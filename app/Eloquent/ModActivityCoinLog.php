<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCoinLog extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coin_log';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
