<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCoinUser extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coin_user';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
