<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCoupon extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coupon';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
