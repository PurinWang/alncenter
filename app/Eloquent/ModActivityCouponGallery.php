<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCouponGallery extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coupon_gallery';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
