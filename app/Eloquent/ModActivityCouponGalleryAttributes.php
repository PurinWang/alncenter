<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCouponGalleryAttributes extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coupon_gallery_attributes';
    protected $primaryKey = 'iSubId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
