<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCouponLog extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_coupon_log';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
