<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityNews extends Model
{
    public $timestamps = false;
    protected $table = 'mod_activity_news';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
