<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModBonus extends Model
{
    public $timestamps = false;
    protected $table = 'mod_bonus';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
