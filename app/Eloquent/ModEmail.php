<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModEmail extends Model
{
    public $timestamps = false;
    protected $table = 'mod_email';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
