<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModInvest extends Model
{
    public $timestamps = false;
    protected $table = 'mod_invest';
    protected $primaryKey = 'vInvestNum';
    public $incrementing = false;

    /*
     *
     */
    public function __construct ()
    {
    }

}
