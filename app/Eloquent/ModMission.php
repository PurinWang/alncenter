<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModMission extends Model
{
    public $timestamps = false;
    protected $table = 'mod_mission';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
