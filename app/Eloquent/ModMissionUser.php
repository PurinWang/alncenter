<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModMissionUser extends Model
{
    public $timestamps = false;
    protected $table = 'mod_mission_user';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
