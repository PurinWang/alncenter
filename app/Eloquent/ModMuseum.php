<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModMuseum extends Model
{
    public $timestamps = false;
    protected $table = 'mod_museum';
    protected $primaryKey = 'iType';

    /*
     *
     */
    public function __construct ()
    {
    }
}
