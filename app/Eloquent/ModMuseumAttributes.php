<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModMuseumAttributes extends Model
{
    public $timestamps = false;
    protected $table = 'mod_museum_attributes';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
