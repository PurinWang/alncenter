<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModOrderMeta extends Model
{
    public $timestamps = false;
    protected $table = 'mod_order_meta';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function getProductInfo ()
    {

    }

}
