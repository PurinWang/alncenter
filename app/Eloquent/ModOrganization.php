<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModOrganization extends Model
{
    public $timestamps = false;
    protected $table = 'mod_organization';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }
}
