<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModOrganizationAttributes extends Model
{
    public $timestamps = false;
    protected $table = 'mod_organization_attributes';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
