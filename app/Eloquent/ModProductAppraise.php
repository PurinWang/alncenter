<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductAppraise extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_appraise';
    protected $primaryKey = 'iId';

}
