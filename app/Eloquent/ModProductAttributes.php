<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductAttributes extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_attributes';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
