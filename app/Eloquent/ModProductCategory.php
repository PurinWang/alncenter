<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductCategory extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_category';
    protected $primaryKey = 'iId';
}
