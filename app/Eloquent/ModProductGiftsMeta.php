<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductGiftsMeta extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_gifts_meta';
    protected $primaryKey = 'iId';

}
