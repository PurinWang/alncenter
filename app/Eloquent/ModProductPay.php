<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductPay extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_pay';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
