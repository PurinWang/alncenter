<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductPurchaseMeta extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_purchase_meta';
    protected $primaryKey = 'iId';

}
