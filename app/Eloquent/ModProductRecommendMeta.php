<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductRecommendMeta extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_recommend_meta';
    protected $primaryKey = 'iId';

}
