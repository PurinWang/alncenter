<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductShipping extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_shipping';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
