<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModProductSpec extends Model
{
    public $timestamps = false;
    protected $table = 'mod_product_spec';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
