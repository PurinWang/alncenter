<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModStore extends Model
{
    public $timestamps = false;
    protected $table = 'mod_store';
    protected $primaryKey = 'iStoreId';

    /*
     *
     */
    public function __construct ()
    {
    }
}
