<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModStoreAttributes extends Model
{
    public $timestamps = false;
    protected $table = 'mod_store_attributes';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
