<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModStorePayInfo extends Model
{
    public $timestamps = false;
    protected $table = 'mod_store_pay_info';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }
}
