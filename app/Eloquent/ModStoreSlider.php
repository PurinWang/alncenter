<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModStoreSlider extends Model
{
    public $timestamps = false;
    protected $table = 'mod_store_slider';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }

}
