<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysEGift extends Model
{
    public $timestamps = false;
    protected $table = 'sys_e_gift';
    protected $primaryKey = 'iId';

}
