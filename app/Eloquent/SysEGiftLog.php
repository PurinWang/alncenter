<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysEGiftLog extends Model
{
    public $timestamps = false;
    protected $table = 'sys_e_gift_log';
    protected $primaryKey = 'iId';

}
