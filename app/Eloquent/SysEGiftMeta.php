<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysEGiftMeta extends Model
{
    public $timestamps = false;
    protected $table = 'sys_e_gift_meta';
    protected $primaryKey = 'iId';

}
