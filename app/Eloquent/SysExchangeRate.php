<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysExchangeRate extends Model
{
    public $timestamps = false;
    protected $table = 'sys_exchange_rate';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }
}
