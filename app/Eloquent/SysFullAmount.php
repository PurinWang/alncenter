<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysFullAmount extends Model
{
    public $timestamps = false;
    protected $table = 'sys_full_amount';
    protected $primaryKey = 'iId';

}
