<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysFullAmountMeta extends Model
{
    public $timestamps = false;
    protected $table = 'sys_full_amount_meta';
    protected $primaryKey = 'iId';

}
