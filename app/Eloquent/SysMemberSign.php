<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysMemberSign extends Model
{
    public $timestamps = false;
    protected $table = 'sys_member_sign';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
    }
}
