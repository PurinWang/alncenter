<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRecommend extends Model
{
    public $timestamps = false;
    protected $table = 'sys_recommend';
    protected $primaryKey = 'iId';

}
