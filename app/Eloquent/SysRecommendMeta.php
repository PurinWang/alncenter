<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRecommendMeta extends Model
{
    public $timestamps = false;
    protected $table = 'sys_recommend_meta';
    protected $primaryKey = 'iId';

}
