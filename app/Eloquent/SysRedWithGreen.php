<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRedWithGreen extends Model
{
    public $timestamps = false;
    protected $table = 'sys_red_with_green';
    protected $primaryKey = 'iId';

}
