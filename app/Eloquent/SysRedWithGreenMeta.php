<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysRedWithGreenMeta extends Model
{
    public $timestamps = false;
    protected $table = 'sys_red_with_green_meta';
    protected $primaryKey = 'iId';

}
