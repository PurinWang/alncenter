<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    //
    public $rtndata = [];

    /*
     *
     */
    public function __construct ()
    {
        $this->rtndata ['status'] = 0;
        $this->rtndata ['message'] = "";
        $this->rtndata ['info'] = [];
    }
}
