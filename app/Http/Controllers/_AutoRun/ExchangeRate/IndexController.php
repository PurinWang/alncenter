<?php
// */5 * * * * curl http://locals.cc/api/bonus/auto_bonus >> /tmp/crontab.log
// 1 * * * * curl http://locals.cc/api/bonus/auto_bonus >> /tmp/crontab.log
// 31 * * * * curl http://locals.cc/api/bonus/auto_bonus >> /tmp/crontab.log

namespace App\Http\Controllers\_AutoRun\ExchangeRate;

use App\Http\Controllers\_AutoRun\_AutoRunController;
use App\LogExchangeRate;
use App\SysExchangeRate;

class IndexController extends _AutoRunController
{
    public $module = [ 'auto_run', 'exchange_rate', 'index' ];

    //

    /*
     *
     */
    public function getExchangeRate ()
    {
        $content = file_get_contents( "https://www.bfxdata.com/json/bfxdataToday.json?_=" . time() );
        $result = json_decode( $content, true );
        $result['priceETHUSD'];

        $map['vRateName'] = 'ETH';
        $Dao = SysExchangeRate::where( $map )->first();
        if ( !$Dao) {
            $Dao = new SysExchangeRate();
            $Dao->vRateName = 'ETH';
            $Dao->fRateValue = $result['priceETHUSD'][0];
            $Dao->fRateValueNow = $result['priceETHUSD'][0];
            $Dao->fRateValueLow = $result['minETHUSD'][0];
            $Dao->fRateValueHigh = $result['maxETHUSD'][0];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->save();
        } else {
            $Dao->fRateValueNow = $result['priceETHUSD'][0];;
            $Dao->fRateValueLow = $result['minETHUSD'][0];
            $Dao->fRateValueHigh = $result['maxETHUSD'][0];
            $Dao->iUpdateTime = time();
            $Dao->save();
        }

        LogExchangeRate::where( [ 'iStatus' => 0 ] )->update( [ 'iStatus' => 1 ] );
        $Dao = new LogExchangeRate();
        $Dao->vRateName = 'ETH';
        $Dao->fBuyCash = $result['askETHUSD'][0];
        $Dao->fSellCash = $result['bidETHUSD'][0];
        $Dao->fBuySpot = $result['askETHUSD'][0];
        $Dao->fSellSpot = $result['bidETHUSD'][0];
        $Dao->iDateTime = mktime( 0, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) );
        $Dao->iStatus = 0;
        $Dao->save();

        $map['vRateName'] = 'BTC';
        $Dao = SysExchangeRate::where( $map )->first();
        if ( !$Dao) {
            $Dao = new SysExchangeRate();
            $Dao->vRateName = 'BTC';
            $Dao->fRateValue = $result['priceBTCUSD'][0];
            $Dao->fRateValueNow = $result['priceBTCUSD'][0];
            $Dao->fRateValueLow = $result['minBTCUSD'][0];
            $Dao->fRateValueHigh = $result['maxBTCUSD'][0];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->save();
        } else {
            $Dao->fRateValueNow = $result['priceBTCUSD'][0];;
            $Dao->fRateValueLow = $result['minBTCUSD'][0];
            $Dao->fRateValueHigh = $result['maxBTCUSD'][0];
            $Dao->iUpdateTime = time();
            $Dao->save();
        }

        LogExchangeRate::where( [ 'iStatus' => 0 ] )->update( [ 'iStatus' => 1 ] );
        $Dao = new LogExchangeRate();
        $Dao->vRateName = 'BTC';
        $Dao->fBuyCash = $result['askBTCUSD'][0];
        $Dao->fSellCash = $result['bidBTCUSD'][0];
        $Dao->fBuySpot = $result['askBTCUSD'][0];
        $Dao->fSellSpot = $result['bidBTCUSD'][0];
        $Dao->iDateTime = mktime( 0, 0, 0, date( 'm' ), date( 'd' ), date( 'Y' ) );
        $Dao->iStatus = 0;
        $Dao->save();

        $this->rtndata ['status'] = 1;

        return response()->json( $this->rtndata );
    }

    /**
     * @param $url 请求网址
     * @param bool $params 请求参数
     * @param int $ispost 请求方式
     * @param int $https https协议
     * @return bool|mixed
     */
    public static function curl ( $url, $https = 1, $ispost = 0, $params = false )
    {
        $httpInfo = [];
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        if ($https) {
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false ); // 对认证证书来源的检查
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false ); // 从证书中检查SSL加密算法是否存在
        }
        if ($ispost) {
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
            curl_setopt( $ch, CURLOPT_URL, $url );
        } else {
            if ($params) {
                if (is_array( $params )) {
                    $params = http_build_query( $params );
                }
                curl_setopt( $ch, CURLOPT_URL, $url . '?' . $params );
            } else {
                curl_setopt( $ch, CURLOPT_URL, $url );
            }
        }

        $response = curl_exec( $ch );

        if ($response === false) {
            //echo "cURL Error: " . curl_error($ch);
            return false;
        }
        $httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo, curl_getinfo( $ch ) );
        curl_close( $ch );

        return $response;
    }
}
