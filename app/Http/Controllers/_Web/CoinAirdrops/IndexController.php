<?php

namespace App\Http\Controllers\_Web\CoinAirdrops;

use App\Http\Controllers\_Web\_WebController;
use App\ModCoinAirdrops;
use Illuminate\Http\Request;

class IndexController extends _WebController
{
    public $module = [ 'coin_airdrops', 'index' ];
    public $iType = [ 801 ];

    /*
      *
      */
    public function index ()
    {
        $this->breadcrumb = [
            $this->module[0] => "#",
            implode( '.', $this->module ) => url( 'web/' . implode( '/', $this->module ) ),
        ];
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        return $this->view;
    }

    /*
     *
     */
    public function getList ( Request $request )
    {
        $search_word = $request->input( 'sSearch' );
        $iDisplayLength = $request->input( 'iDisplayLength' );
        $iDisplayStart = $request->input( 'iDisplayStart' );
        $sEcho = $request->input( 'sEcho' );
        $sort_arr = explode( ',', $request->input( 'sColumns' ) );
        $sort_name = $sort_arr[$request->input( 'iSortCol_0' )];
        $sort_dir = $request->input( 'sSortDir_0' );
        //remove null
        $sort_arr = array_filter( $sort_arr );

        $total_count = ModCoinAirdrops::where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( 'bDel', 0 )->count();

        $data_arr = ModCoinAirdrops::where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( 'bDel', 0 )->orderBy( $sort_name, $sort_dir )->skip( $iDisplayStart )->take( $iDisplayLength )->get();

        foreach ($data_arr as $key => $var) {
            $var->DT_RowId = $var->iId;
            $var->iCreateTime = date( 'Y/m/d H:i:s', $var->iCreateTime );
            $var->iStartTime = date( 'Y/m/d', $var->iStartTime );
            $var->iEndTime = date( 'Y/m/d', $var->iEndTime );
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['sEcho'] = $sEcho;
        $this->rtndata ['iTotalDisplayRecords'] = $total_count;
        $this->rtndata ['iTotalRecords'] = $total_count;
        $this->rtndata ['aaData'] = $data_arr;

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doSave ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModCoinAirdrops::find( $id );
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        if ($request->exists( 'iType' )) {
            $Dao->iType = $request->input( 'iType' );
        }
        if ($request->exists( 'vTitle' )) {
            $Dao->vTitle = $request->input( 'vTitle' );
        }
        if ($request->exists( 'vSummary' )) {
            $Dao->vSummary = $request->input( 'vSummary' );
        }
        if ($request->exists( 'vImages' )) {
            $Dao->vImages = $request->input( 'vImages' );
        }
        if ($request->exists( 'vUrl' )) {
            $Dao->vUrl = $request->input( 'vUrl' );
        }
        if ($request->exists( 'vDetail' )) {
            $Dao->vDetail = $request->input( 'vDetail' );
        }
        if ($request->exists( 'iBonus' )) {
            $Dao->iBonus = $request->input( 'iBonus' );
        }
        if ($request->exists( 'iLimitBonus' )) {
            $Dao->iLimitBonus = $request->input( 'iLimitBonus' );
        }
        if ($request->exists( 'iStartTime' )) {
            $Dao->iStartTime =  strtotime( $request->input( 'iStartTime' ) ) + 79200;
        }
        if ($request->exists( 'iEndTime' )) {
            $Dao->iEndTime = strtotime( $request->input( 'iEndTime' ) ) + 86399;
        }
        if ($request->exists( 'iStatus' )) {
            $Dao->iStatus = ( $request->input( 'iStatus' ) == "change" ) ? !$Dao->iStatus : $request->input( 'iStatus' );
        }
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'edit', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doAdd ( Request $request )
    {
        $maxRank = ModCoinAirdrops::max( 'iRank' );
        $Dao = new ModCoinAirdrops();
        $Dao->iMemberId = session()->get( 'member.iId' );
        $Dao->iType = ( $request->exists( 'iType' ) ) ? $request->input( 'iType' ) : 1;
        $Dao->vTitle = ( $request->exists( 'vTitle' ) ) ? $request->input( 'vTitle' ) : "";
        $Dao->vSummary = ( $request->exists( 'vSummary' ) ) ? $request->input( 'vSummary' ) : "";
        $Dao->vImages = ( $request->exists( 'vImages' ) ) ? $request->input( 'vImages' ) : config( 'config.empty_image' );
        $Dao->vUrl = ( $request->exists( 'vUrl' ) ) ? $request->input( 'vUrl' ) : "#";
        $Dao->vDetail = ( $request->exists( 'vDetail' ) ) ? $request->input( 'vDetail' ) : "";
        $Dao->iBonus = ( $request->exists( 'iBonus' ) ) ? $request->input( 'iBonus' ) : 0;
        $Dao->iLimitBonus = ( $request->exists( 'iLimitBonus' ) ) ? $request->input( 'iLimitBonus' ) : 0;
        $Dao->iStartTime = ( $request->exists( 'iStartTime' ) ) ? strtotime( $request->input( 'iStartTime' ) ) + 79200 : time();
        $Dao->iEndTime = ( $request->exists( 'iEndTime' ) ) ? strtotime( $request->input( 'iEndTime' ) ) + 86399 : time();
        $Dao->iRank = $maxRank + 1;
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        $Dao->iStatus = ( $request->exists( 'iStatus' ) ) ? $request->input( 'iStatus' ) : 0;
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
            $this->rtndata ['rtnurl'] = url( 'web/' . implode( '/', $this->module ) );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'add', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doDel ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModCoinAirdrops::find( $id );
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao->bDel = 1;
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.delete_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'delete', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.delete_fail' );
        }

        return response()->json( $this->rtndata );
    }
}

