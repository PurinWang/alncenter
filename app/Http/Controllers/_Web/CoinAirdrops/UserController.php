<?php

namespace App\Http\Controllers\_Web\CoinAirdrops;

use App\Http\Controllers\_Web\_WebController;
use App\ModCoinAirdrops;
use App\ModCoinAirdropsUser;
use Illuminate\Http\Request;

class UserController extends _WebController
{
    public $module = [ 'coin_airdrops', 'user' ];
    public $iType = [ 802 ];

    /*
     *
     */
    public function index ()
    {
        $this->breadcrumb = [
            $this->module[0] => "#",
            implode( '.', $this->module ) => url( 'web/' . implode( '/', $this->module ) ),
        ];
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        //
        $total = [];
        $total['ALLN'] = ModCoinAirdropsUser::where( 'iStatus', 1 )->sum( 'iGetBonus' );
        $this->view->with( 'total', $total );

        //
        $mapCoinAirdrops['iStatus'] = 1;
        $mapCoinAirdrops['bDel'] = 0;
        $DaoCoinAirdrops = ModCoinAirdrops::where( $mapCoinAirdrops )->get();
        $this->view->with( 'CoinAirdrops', $DaoCoinAirdrops );

        return $this->view;
    }

    /*
     *
     */
    public function getList ( Request $request )
    {
        $search_word = $request->input( 'sSearch' );
        $iDisplayLength = $request->input( 'iDisplayLength' );
        $iDisplayStart = $request->input( 'iDisplayStart' );
        $sEcho = $request->input( 'sEcho' );
        $sort_arr = explode( ',', $request->input( 'sColumns' ) );
        $sort_name = $sort_arr[$request->input( 'iSortCol_0' )];
        $sort_dir = $request->input( 'sSortDir_0' );
        //remove null
        $sort_arr = array_filter( $sort_arr );
        //
        $orderStartDate = $request->exists( 'orderStartDate' ) ? strtotime( $request->input( 'orderStartDate' ) ) : 0;
        $orderEndDate = $request->exists( 'orderEndDate' ) ? strtotime( $request->input( 'orderEndDate' ) ) : 0;

        $total_count = ModCoinAirdropsUser::join( 'sys_member_info', function( $join ) {
            $join->on( 'sys_member_info.iMemberId', '=', 'mod_mission_user.iMemberId' );
        } )->where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( function( $query ) use ( $orderStartDate, $orderEndDate ) {
            if ($orderStartDate && $orderEndDate) {
                $query->whereBetween( 'mod_mission_user.iCreateTime', [ $orderStartDate, $orderEndDate ] );
            }
        } )->where( 'bDel', 0 )->count();

        $data_arr = ModCoinAirdropsUser::join( 'sys_member_info', function( $join ) {
            $join->on( 'sys_member_info.iMemberId', '=', 'mod_mission_user.iMemberId' );
        } )->where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( function( $query ) use ( $orderStartDate, $orderEndDate ) {
            if ($orderStartDate && $orderEndDate) {
                $query->whereBetween( 'mod_mission_user.iCreateTime', [ $orderStartDate, $orderEndDate ] );
            }
        } )->where( 'bDel', 0 )->orderBy( $sort_name, $sort_dir )->skip( $iDisplayStart )->take( $iDisplayLength )->get();

        foreach ($data_arr as $key => $var) {
            $var->DT_RowId = $var->iId;
            $var->mission_title = ModCoinAirdrops::where( 'iId', $var->iMissionId )->value( 'vTitle' );
            $var->iCreateTime = date( 'Y/m/d H:i:s', $var->iCreateTime );
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['sEcho'] = $sEcho;
        $this->rtndata ['iTotalDisplayRecords'] = $total_count;
        $this->rtndata ['iTotalRecords'] = $total_count;
        $this->rtndata ['aaData'] = $data_arr;

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doSave ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModCoinAirdropsUser::where( 'iId', $id )->first();
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        if ($request->exists( 'iGetBonus' )) {
            $Dao->iGetBonus = $request->input( 'iGetBonus' );
        }
        if ($request->exists( 'iStatus' )) {
            $Dao->iStatus = ( $Dao->iStatus ) ? 0 : 1;
        }
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'edit', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doAdd ( Request $request )
    {
        $Dao = new ModCoinAirdropsUser();
        $Dao->iMissionId = ( $request->exists( 'iMissionId' ) ) ? $request->input( 'iMissionId' ) : 1;
        $Dao->iMemberId = ( $request->exists( 'iMemberId' ) ) ? $request->input( 'iMemberId' ) : 1;
        $Dao->iGetBonus = ( $request->exists( 'iBonus' ) ) ? $request->input( 'iBonus' ) : 0;
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        $Dao->iStatus = ( $request->exists( 'iStatus' ) ) ? $request->input( 'iStatus' ) : 0;
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
            $this->rtndata ['rtnurl'] = url( 'web/' . implode( '/', $this->module ) );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'add', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
    *
    */
    public function doDel ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModCoinAirdropsUser::where( 'iId', $id )->first();
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao->bDel = 1;
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.delete_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'delete', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.delete_fail' );
        }

        return response()->json( $this->rtndata );
    }
}

