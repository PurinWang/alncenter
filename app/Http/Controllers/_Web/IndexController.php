<?php

namespace App\Http\Controllers\_Web;

use App\Http\Controllers\_Web\_WebController;

class IndexController extends _WebController
{
    public $module = [ 'index' ];
    protected $accessToken = 'access_token$production$jvvgcnnbwf8q7ykh$1ddf732278c682686df8a71211fc044e';

    /*
     *
     */
    public function index ()
    {
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        return $this->view;
    }
}
