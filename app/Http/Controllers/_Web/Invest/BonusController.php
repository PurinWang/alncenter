<?php

namespace App\Http\Controllers\_Web\Invest;

use App\Http\Controllers\_Web\_WebController;
use App\ModBonus;
use Illuminate\Http\Request;

class BonusController extends _WebController
{
    public $module = [ 'invest', 'bonus' ];
    public $iType = [ 702 ];

    /*
     *
     */
    public function index ()
    {
        $this->breadcrumb = [
            $this->module[0] => "#",
            implode( '.', $this->module ) => url( 'web/' . implode( '/', $this->module ) ),
        ];
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        $total = [];
        $total['ALLN'] = ModBonus::where( 'iStatus', 1 )->sum( 'iBonus' );
        $this->view->with( 'total', $total );

        return $this->view;
    }

    /*
     *
     */
    public function getList ( Request $request )
    {
        $search_word = $request->input( 'sSearch' );
        $iDisplayLength = $request->input( 'iDisplayLength' );
        $iDisplayStart = $request->input( 'iDisplayStart' );
        $sEcho = $request->input( 'sEcho' );
        $sort_arr = explode( ',', $request->input( 'sColumns' ) );
        $sort_name = $sort_arr[$request->input( 'iSortCol_0' )];
        $sort_dir = $request->input( 'sSortDir_0' );
        //remove null
        $sort_arr = array_filter( $sort_arr );
        //
        $orderStartDate = $request->exists( 'orderStartDate' ) ? strtotime( $request->input( 'orderStartDate' ) ) : 0;
        $orderEndDate = $request->exists( 'orderEndDate' ) ? strtotime( $request->input( 'orderEndDate' ) ) : 0;

        $total_count = ModBonus::join( 'sys_member_info', function( $join ) {
            $join->on( 'sys_member_info.iMemberId', '=', 'mod_bonus.iMemberId' );
        } )->where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( function( $query ) use ( $orderStartDate, $orderEndDate ) {
            if ($orderStartDate && $orderEndDate) {
                $query->whereBetween( 'mod_bonus.iCreateTime', [ $orderStartDate, $orderEndDate ] );
            }
        } )->where( 'bDel', 0 )->count();

        $data_arr = ModBonus::join( 'sys_member_info', function( $join ) {
            $join->on( 'sys_member_info.iMemberId', '=', 'mod_bonus.iMemberId' );
        } )->where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( function( $query ) use ( $orderStartDate, $orderEndDate ) {
            if ($orderStartDate && $orderEndDate) {
                $query->whereBetween( 'mod_bonus.iCreateTime', [ $orderStartDate, $orderEndDate ] );
            }
        } )->where( 'bDel', 0 )->orderBy( $sort_name, $sort_dir )->skip( $iDisplayStart )->take( $iDisplayLength )->get();

        foreach ($data_arr as $key => $var) {
            $var->DT_RowId = $var->iId;
            $var->iCreateTime = date( 'Y/m/d H:i:s', $var->iCreateTime );
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['sEcho'] = $sEcho;
        $this->rtndata ['iTotalDisplayRecords'] = $total_count;
        $this->rtndata ['iTotalRecords'] = $total_count;
        $this->rtndata ['aaData'] = $data_arr;

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doSave ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModBonus::where( 'iId', $id )->first();
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        if ($request->exists( 'iMemberId' )) {
            $Dao->iMemberId = $request->input( 'iMemberId' );
        }
        if ($request->exists( 'vType' )) {
            $Dao->vType = $request->input( 'vType' );
        }
        if ($request->exists( 'iBonus' )) {
            $Dao->iBonus = $request->input( 'iBonus' );
        }
        if ($request->exists( 'vBonusNum' )) {
            $Dao->vBonusNum = $request->input( 'vBonusNum' );
        }
        if ($request->exists( 'iStatus' )) {
            $Dao->iStatus = ( $Dao->iStatus ) ? 0 : 1;
        }
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'edit', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doAdd ( Request $request )
    {
        $Dao = new ModBonus();
        $Dao->iMemberId = ( $request->exists( 'iMemberId' ) ) ? $request->input( 'iMemberId' ) : 1;
        $Dao->vType = ( $request->exists( 'vType' ) ) ? $request->input( 'vType' ) : "";
        $Dao->iBonus = ( $request->exists( 'iBonus' ) ) ? $request->input( 'iBonus' ) : 0;
        $Dao->vBonusNum = ( $request->exists( 'vBonusNum' ) ) ? $request->input( 'vBonusNum' ) : "";
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        $Dao->iStatus = ( $request->exists( 'iStatus' ) ) ? $request->input( 'iStatus' ) : 0;
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
            $this->rtndata ['rtnurl'] = url( 'web/' . implode( '/', $this->module ) );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'add', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
    *
    */
    public function doDel ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModBonus::where( 'iId', $id )->first();
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao->bDel = 1;
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.delete_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'delete', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.delete_fail' );
        }

        return response()->json( $this->rtndata );
    }
}

