<?php

namespace App\Http\Controllers\_Web\Invest;

class IndexETHController extends _InvestController
{
    public $module = [ 'invest', 'index_1' ];
    public $iType = [ 703 ];
    public $vType = "ETH";
}

