<?php

namespace App\Http\Controllers\_Web\Invest;

class IndexUSDController extends _InvestController
{
    public $module = [ 'invest', 'index_3' ];
    public $iType = [ 705 ];
    public $vType = "USD";
}

