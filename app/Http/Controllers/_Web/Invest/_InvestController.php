<?php

namespace App\Http\Controllers\_Web\Invest;

use App\Http\Controllers\_Web\_WebController;
use App\ModInvest;
use Illuminate\Http\Request;

class _InvestController extends _WebController
{
    /*
     *
     */
    public function index ()
    {
        $this->breadcrumb = [
            $this->module[0] => "#",
            implode( '.', $this->module ) => url( 'web/' . implode( '/', $this->module ) ),
        ];
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        $total = [];
        $total['ALLN'] = ModInvest::where( 'iStatus', '<>', 2 )->sum( 'iTotal' );
        $total['ETH'] = ModInvest::where( 'iStatus', '<>', 2 )->where( 'vType', 'ETH' )->sum( 'iCount' );
        $total['BTC'] = ModInvest::where( 'iStatus', '<>', 2 )->where( 'vType', 'BTC' )->sum( 'iCount' );
        $total['USD'] = ModInvest::where( 'iStatus', '<>', 2 )->where( 'vType', 'USD' )->sum( 'iCount' );
        $total['CASH'] = ModInvest::where( 'iStatus', '<>', 2 )->where( 'vType', 'CASH' )->sum( 'iCount' );
        $this->view->with( 'total', $total );

        return $this->view;
    }

    /*
     *
     */
    public function getList ( Request $request )
    {
        $search_word = $request->input( 'sSearch' );
        $iDisplayLength = $request->input( 'iDisplayLength' );
        $iDisplayStart = $request->input( 'iDisplayStart' );
        $sEcho = $request->input( 'sEcho' );
        $sort_arr = explode( ',', $request->input( 'sColumns' ) );
        $sort_name = $sort_arr[$request->input( 'iSortCol_0' )];
        $sort_dir = $request->input( 'sSortDir_0' );
        //remove null
        $sort_arr = array_filter( $sort_arr );
        //
        $orderStartDate = $request->exists( 'orderStartDate' ) ? strtotime( $request->input( 'orderStartDate' ) ) : 0;
        $orderEndDate = $request->exists( 'orderEndDate' ) ? strtotime( $request->input( 'orderEndDate' ) ) + 86399 : 0;
        $ALLN_MIN = $request->exists( 'alln_min' ) ? $request->input( 'alln_min' ) : 0;
        $ALLN_MAX = $request->exists( 'alln_max' ) ? $request->input( 'alln_max' ) : 0;
        $ETH_MIN = $request->exists( 'eth_min' ) ? $request->input( 'eth_min' ) : 0;
        $ETH_MAX = $request->exists( 'eth_max' ) ? $request->input( 'eth_max' ) : 0;

        $total_count = ModInvest::join( 'sys_member_info', function( $join ) {
            $join->on( 'sys_member_info.iMemberId', '=', 'mod_invest.iMemberId' );
        } )->where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( function( $query ) use ( $orderStartDate, $orderEndDate, $ALLN_MIN, $ALLN_MAX, $ETH_MIN, $ETH_MAX ) {
            if ($orderStartDate && $orderEndDate) {
                $query->whereBetween( 'mod_invest.iCreateTime', [ $orderStartDate, $orderEndDate ] );
            }
            if ($ALLN_MIN && $ALLN_MAX) {
                $query->whereBetween( 'mod_invest.iTotal', [ $ALLN_MIN, $ALLN_MAX ] );
            }
            if ($ETH_MIN && $ETH_MAX) {
                $query->whereBetween( 'mod_invest.iCount', [ $ETH_MIN, $ETH_MAX ] );
            }
        } )->where( 'vType', $this->vType )->where( 'iStatus', '<>', 2 )->count();

        $data_arr = ModInvest::join( 'sys_member_info', function( $join ) {
            $join->on( 'sys_member_info.iMemberId', '=', 'mod_invest.iMemberId' );
        } )->where( function( $query ) use ( $sort_arr, $search_word ) {
            foreach ($sort_arr as $item) {
                $query->orWhere( $item, 'like', '%' . $search_word . '%' );
            }
        } )->where( function( $query ) use ( $orderStartDate, $orderEndDate, $ALLN_MIN, $ALLN_MAX, $ETH_MIN, $ETH_MAX ) {
            if ($orderStartDate && $orderEndDate) {
                $query->whereBetween( 'mod_invest.iCreateTime', [ $orderStartDate, $orderEndDate ] );
            }
            if ($ALLN_MIN && $ALLN_MAX) {
                $query->whereBetween( 'mod_invest.iTotal', [ $ALLN_MIN, $ALLN_MAX ] );
            }
            if ($ETH_MIN && $ETH_MAX) {
                $query->whereBetween( 'mod_invest.iCount', [ $ETH_MIN, $ETH_MAX ] );
            }
        } )->where( 'vType', $this->vType )->where( 'iStatus', '<>', 2 )->orderBy( $sort_name, $sort_dir )->skip( $iDisplayStart )->take( $iDisplayLength )->get();

        foreach ($data_arr as $key => $var) {
            $var->DT_RowId = $var->iId;
            $var->iCreateTime = date( 'Y/m/d H:i:s', $var->iCreateTime );
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['sEcho'] = $sEcho;
        $this->rtndata ['iTotalDisplayRecords'] = $total_count;
        $this->rtndata ['iTotalRecords'] = $total_count;
        $this->rtndata ['aaData'] = $data_arr;

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doSave ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModInvest::where( 'iId', $id )->first();
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        if ($request->exists( 'iCount' )) {
            $Dao->iCount = $request->input( 'iCount' );
        }
        if ($request->exists( 'iTotal' )) {
            $Dao->iTotal = $request->input( 'iTotal' );
        }
        if ($request->exists( 'iStatus' )) {
            $Dao->iStatus = ( $Dao->iStatus ) ? 0 : 1;
        }
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'edit', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
    *
    */
    public function doDel ( Request $request )
    {
        $id = ( $request->exists( 'iId' ) ) ? $request->input( 'iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModInvest::where( 'iId', $id )->first();
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao->iStatus = 2;
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.delete_success' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'delete', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.delete_fail' );
        }

        return response()->json( $this->rtndata );
    }
}

