<?php
// debugbar()->info($this->func);
// debugbar()->error('Error!');
// debugbar()->warning('Watch out…');
// debugbar()->addMessage('Another message', 'mylabel');
namespace App\Http\Controllers\_Web\LOG\L01;

use App\Http\Controllers\_Web\LOG\LOGController;
use App\LogLogin;
use Illuminate\Http\Request;

class IndexController extends LOGController
{
    public $module = [ 'log', 'log01' ];
    public $get_list_api = "";

    /*
     *
     */
    public function index ()
    {
        $this->breadcrumb = [
            $this->module[0] => "#",
            implode( '.', $this->module ) => url( 'web/' . implode( '/', $this->module ) )
        ];
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        return $this->view;
    }

    /*
     *
     */
    function getList ( Request $request )
    {
        $start_time = $request->exists( 's_time' ) ? strtotime( $request->input( 's_time' ) ) : strtotime( date( 'Y-m-d' ) );
        $end_time = $request->exists( 'e_time' ) ? strtotime( $request->input( 'e_time' ) ) + 86400 : time();
        $data_arr = LogLogin::leftJoin( 'mod_store', function( $join ) {
            $join->on( 'mod_store.iStoreId', '=', 'log_login.iStoreId' );
        } )->whereBetween( 'iDateTime', [ $start_time, $end_time ] )->groupBy( 'iStoreId', 'vStoreName' )->selectRaw( 'log_login.iStoreId , mod_store.vStoreName , count(*) as total' )->get();
        foreach ($data_arr as $key => $var) {
            $var->DT_RowId = $var->iId;
            $var->iDateTime = date( 'Y/m/d H:i:s', $var->iDateTime );
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['aaData'] = $data_arr;

        return response()->json( $this->rtndata );
    }

    /*
    *
    */
    function doAdd ( Request $request )
    {
        $iStoreId = $request->input( 'iStoreId' );
        $iMemberId = $request->input( 'iMemberId' );
        $iDateTime = strtotime( $request->input( 'iDateTime' ) );
        $iCount = $request->input( 'iCount' );

        for ($i = 0 ; $i < $iCount ; $i++) {
            $iDateTime = $iDateTime + rand( 100, 1800 ) * $i;
            $Dao = new LogLogin();
            $Dao->iStoreId = $iStoreId;
            $Dao->iMemberId = $iMemberId;
            $Dao->vAction = "login";
            $Dao->iDateTime = $iDateTime;
            $Dao->vIP = \Illuminate\Support\Facades\Request::ip();
            $Dao->save();
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['message'] = trans( '_web_message.add_success' );
        $this->rtndata ['rtnurl'] = url( 'web/' . implode( '/', $this->module ) );

        return response()->json( $this->rtndata );
    }
}
