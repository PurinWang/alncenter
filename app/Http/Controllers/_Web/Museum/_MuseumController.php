<?php

namespace App\Http\Controllers\_Web\Museum;

use App\Http\Controllers\_Web\_WebController;
use App\ModMuseum;
use Illuminate\Http\Request;

class _MuseumController extends _WebController
{
    /*
     *
     */
    public function index ()
    {
        $this->breadcrumb = [
            $this->module[0] => "#",
            implode( '.', $this->module ) => url( 'web/' . implode( '/', $this->module ) )
        ];
        $this->func = "web." . implode( '.', $this->module );
        $this->__initial();

        $Dao = ModMuseum::find( $this->iType );
        $this->view->with( 'info', $Dao );

        return $this->view;
    }

    /*
     *
     */
    public function doSave ( Request $request )
    {
        $Dao = ModMuseum::find( $this->iType );
        if ($request->exists( 'vMuseumCode' )) {
            $Dao->vMuseumCode = $request->input( 'vMuseumCode' );
        };
        if ($request->exists( 'vMuseumName' )) {
            $Dao->vMuseumName = $request->input( 'vMuseumName' );
        };
        if ($request->exists( 'vMuseumSummary' )) {
            $Dao->vMuseumSummary = $request->input( 'vMuseumSummary' );
        };
        if ($request->exists( 'vMuseumLogo' )) {
            $Dao->vMuseumLogo = $request->input( 'vMuseumLogo' );
        };
        if ($request->exists( 'bShow' )) {
            $Dao->bShow = $request->input( 'bShow' );
        };
        if ($request->exists( 'bOpen' )) {
            $Dao->bOpen = $request->input( 'bOpen' );
        };
        $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.edit.success' );

            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'edit', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.edit.fail' );
        }

        return response()->json( $this->rtndata );
    }
}

