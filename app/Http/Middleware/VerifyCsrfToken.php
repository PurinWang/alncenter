<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'payservice/spgateway/notifyurl_service',
        'payservice/spgateway/return_url',
        'payservice/spgateway/customer_url',
        //
        'payservice/spgateway/bonus/notifyurl_service',
        'payservice/spgateway/bonus/return_url',
        'payservice/spgateway/bonus/customer_url',

    ];
}
