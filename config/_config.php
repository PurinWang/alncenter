<?php
return [
    'store_userid' => [ 240001, 249999 ],
    'platform_fee' => 0.2, //平台分潤
    'admin_access' => [ 1, 2 ],
    'agent_code' => "pin2wall",
    'file_path' => 'upload/userdata/',
    'mall_host' => env( 'MALL_HOST' ),
    'sys_category' => [
        'product' => [
            'type' => 1,
            'pid' => 1,
        ],
        'pay' => [
            'type' => 2,
            'pid' => 2,
        ],
        'news' => [
            'type' => 3,
            'pid' => 3,
        ],
        'activity' => [
            'type' => 4,
            'pid' => 4,
        ],
        'museum' => [
            'type' => 5,
            'pid' => 5,
        ],
    ],
    'activity' => [
        'coin' => [
            '1' => '註冊送飛幣',
            '2' => '登入送飛幣',
            '3' => '消費滿額送飛幣'
        ]
    ],
    'exchange_rate_api' => 'http://asper-bot-rates.appspot.com/currency.json',
    'current_lang' => 'zh-tw',
    'manager_access' => [ 1, 2 ],
    'order_limit_time' => env( 'ORDER_LIMIT_TIME', 43200 ), //訂單有效週期12小時，超過未付款即作廢
    'verification' => [
        'limit' => 12,
        'time' => 3600,
    ],
    'museum' => [
        'a01' => '生鮮蔬果',
        'a02' => '精緻糕品',
        'a03' => '果醋飲品',
        'a04' => '伴手禮系列',
        'a05' => '生猛海鮮',
        'a06' => '在地鮮食',
        'a07' => '醇釀酒品',
        'a08' => '醃製食品',
        'a09' => '工藝精品',
        'a10' => '小農鮮乳',
        'a11' => '特色美食',
        'a12' => '其他',
    ],
    'lang' => [
        'zh-tw' => '繁體中文',
        'zh-cn' => '簡體中文',
        'en' => '英文',
        'ja' => '日文',
        'ko' => '韓文',
    ]
];
