<?php
return [
    "web" =>
        [
            "index" =>
                [
                    "view" => "_template_web.index"
                ],
            "member" =>
                [
                    "userinfo" =>
                        [
                            "view" => "_template_web._member.userinfo"
                        ]
                ],
            "admin" =>
                [
                    "member" =>
                        [
                            "customer" =>
                                [
                                    "view" => "_template_web._admin.member.customer.index",
                                    "menu_access" => "111",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "11"
                                        ],
                                    "access" =>
                                        [
                                            "view" => "_template_web._admin.member.customer.access",
                                            "menu_access" => "111",
                                            "menu_parent" =>
                                                [
                                                    "1",
                                                    "11"
                                                ],
                                        ],
                                ],
                            "employee" =>
                                [
                                    "view" => "_template_web._admin.member.employee.index",
                                    "menu_access" => "112",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "11"
                                        ],
                                    "access" =>
                                        [
                                            "view" => "_template_web._admin.member.employee.access",
                                            "menu_access" => "112",
                                            "menu_parent" =>
                                                [
                                                    "1",
                                                    "11"
                                                ],
                                        ],
                                ],
                            "store" =>
                                [
                                    "view" => "_template_web._admin.member.store.index",
                                    "menu_access" => "113",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "11"
                                        ],
                                    "access" =>
                                        [
                                            "view" => "_template_web._admin.member.store.access",
                                            "menu_access" => "113",
                                            "menu_parent" =>
                                                [
                                                    "1",
                                                    "11"
                                                ],
                                        ],
                                ],
                            "blogger" =>
                                [
                                    "view" => "_template_web._admin.member.blogger.index",
                                    "menu_access" => "114",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "11"
                                        ],
                                    "access" =>
                                        [
                                            "view" => "_template_web._admin.member.blogger.access",
                                            "menu_access" => "114",
                                            "menu_parent" =>
                                                [
                                                    "1",
                                                    "11"
                                                ],
                                        ],
                                ],
                            "supplier" =>
                                [
                                    "view" => "_template_web._admin.member.supplier.index",
                                    "menu_access" => "115",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "11"
                                        ],
                                    "access" =>
                                        [
                                            "view" => "_template_web._admin.member.supplier.access",
                                            "menu_access" => "115",
                                            "menu_parent" =>
                                                [
                                                    "1",
                                                    "11"
                                                ],
                                        ],
                                ],
                        ],
                    "group" =>
                        [
                            "customer" =>
                                [
                                    "view" => "_template_web._admin.group.customer",
                                    "menu_access" => "121",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "12"
                                        ],
                                ],
                            "employee" =>
                                [
                                    "view" => "_template_web._admin.group.employee",
                                    "menu_access" => "122",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "12"
                                        ],
                                ],
                            "store" =>
                                [
                                    "view" => "_template_web._admin.group.store",
                                    "menu_access" => "123",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "12"
                                        ],
                                ],
                            "blogger" =>
                                [
                                    "view" => "_template_web._admin.group.blogger",
                                    "menu_access" => "124",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "12"
                                        ],
                                ],
                            "supplier" =>
                                [
                                    "view" => "_template_web._admin.group.supplier",
                                    "menu_access" => "125",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "12"
                                        ],
                                ],
                        ],
                    "exchange_rate" =>
                        [
                            "index" =>
                                [
                                    "view" => "_template_web._admin.exchange_rate.index",
                                    "menu_access" => "131",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "13"
                                        ],
                                ],
                            "log" =>
                                [
                                    "view" => "_template_web._admin.exchange_rate.log",
                                    "menu_access" => "132",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "13"
                                        ],
                                ],
                        ],
                    "category" =>
                        [
                            "view" => "_template_web._admin.category.index",
                            "menu_access" => "14",
                            "menu_parent" =>
                                [
                                    "1",
                                    "0"
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web._admin.category.sub",
                                    "menu_access" => "14",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "0"
                                        ],
                                ]
                        ],
                    "config" =>
                        [
                            "view" => "_template_web._admin.config.index",
                            "menu_access" => "17",
                            "menu_parent" =>
                                [
                                    "1",
                                    "0"
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web._admin.config.sub",
                                    "menu_access" => "17",
                                    "menu_parent" =>
                                        [
                                            "1",
                                            "0"
                                        ],
                                ]
                        ],
                ],
            "museum" =>
                [
                    "a01" =>
                        [
                            "view" => "_template_web.museum.a01.index",
                            "menu_access" => "201",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a02" =>
                        [
                            "view" => "_template_web.museum.a02.index",
                            "menu_access" => "202",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a03" =>
                        [
                            "view" => "_template_web.museum.a03.index",
                            "menu_access" => "203",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a04" =>
                        [
                            "view" => "_template_web.museum.a04.index",
                            "menu_access" => "204",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a05" =>
                        [
                            "view" => "_template_web.museum.a05.index",
                            "menu_access" => "205",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a06" =>
                        [
                            "view" => "_template_web.museum.a06.index",
                            "menu_access" => "206",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a07" =>
                        [
                            "view" => "_template_web.museum.a07.index",
                            "menu_access" => "207",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a08" =>
                        [
                            "view" => "_template_web.museum.a08.index",
                            "menu_access" => "208",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a09" =>
                        [
                            "view" => "_template_web.museum.a09.index",
                            "menu_access" => "209",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a10" =>
                        [
                            "view" => "_template_web.museum.a10.index",
                            "menu_access" => "210",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a11" =>
                        [
                            "view" => "_template_web.museum.a11.index",
                            "menu_access" => "211",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                    "a12" =>
                        [
                            "view" => "_template_web.museum.a12.index",
                            "menu_access" => "212",
                            "menu_parent" =>
                                [
                                    "2",
                                    "0"
                                ],
                        ],
                ],
            "home" =>
                [
                    "white_paper" =>
                        [
                            "view" => "_template_web.home.white_paper.index",
                            "menu_access" => "601",
                            "menu_parent" =>
                                [
                                    "6",
                                    "0"
                                ],
                        ],
                    "activity" =>
                        [
                            "view" => "_template_web.home.activity.index",
                            "menu_access" => "602",
                            "menu_parent" =>
                                [
                                    "6",
                                    "0"
                                ],
                        ],
                    "milestone" =>
                        [
                            "view" => "_template_web.home.milestone.index",
                            "menu_access" => "603",
                            "menu_parent" =>
                                [
                                    "6",
                                    "0"
                                ],
                        ],
                    "light_paper" =>
                        [
                            "view" => "_template_web.home.light_paper.index",
                            "menu_access" => "604",
                            "menu_parent" =>
                                [
                                    "6",
                                    "0"
                                ],
                        ],
                ],
            "invest" =>
                [
                    "index" =>
                        [
                            "view" => "_template_web.invest.index.index",
                            "menu_access" => "701",
                            "menu_parent" =>
                                [
                                    "7",
                                    "0"
                                ],
                        ],
                    "bonus" =>
                        [
                            "view" => "_template_web.invest.bonus.index",
                            "menu_access" => "702",
                            "menu_parent" =>
                                [
                                    "7",
                                    "0"
                                ],
                        ],
                    "index_1" =>
                        [
                            "view" => "_template_web.invest.index_1.index",
                            "menu_access" => "703",
                            "menu_parent" =>
                                [
                                    "7",
                                    "0"
                                ],
                        ],
                    "index_2" =>
                        [
                            "view" => "_template_web.invest.index_2.index",
                            "menu_access" => "704",
                            "menu_parent" =>
                                [
                                    "7",
                                    "0"
                                ],
                        ],
                    "index_3" =>
                        [
                            "view" => "_template_web.invest.index_3.index",
                            "menu_access" => "705",
                            "menu_parent" =>
                                [
                                    "7",
                                    "0"
                                ],
                        ],
                ],
            "coin_airdrops" =>
                [
                    "index" =>
                        [
                            "view" => "_template_web.coin_airdrops.index",
                            "menu_access" => "801",
                            "menu_parent" =>
                                [
                                    "8",
                                    "0"
                                ],
                        ],
                    "user" =>
                        [
                            "view" => "_template_web.coin_airdrops.user",
                            "menu_access" => "802",
                            "menu_parent" =>
                                [
                                    "8",
                                    "0"
                                ],
                        ],
                ],
            /******************************
             * 訊息公告
             ******************************/
            "news" =>
                [
                    "index" =>
                        [
                            "view" => "_template_web.news.index.index",
                            "menu_access" => "40011",
                            "menu_parent" =>
                                [
                                    "4001",
                                    "0"
                                ],
                        ],
                ],
            /******************************
             * Service
             ******************************/
            "service" =>
                [
                    "message" =>
                        [
                            "view" => "_template_web.service.message.index",
                            "menu_access" => "50011",
                            "menu_parent" =>
                                [
                                    "5001",
                                    "0"
                                ],
                        ],
                    "email" =>
                        [
                            "view" => "_template_web.service.email.index",
                            "menu_access" => "50012",
                            "menu_parent" =>
                                [
                                    "5001",
                                    "0"
                                ],
                        ],
                ],
            /******************************
             * LOG
             ******************************/
            "log" =>
                [
                    "log01" =>
                        [
                            "view" => "_template_web.log.log01.index",
                            "menu_access" => "9900101",
                            "menu_parent" =>
                                [
                                    "99001",
                                    "0"
                                ],
                        ],
                ],
        ]
];