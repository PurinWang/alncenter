<?php
// php artisan make:migration create_sys_category_table
// php artisan migrate
// php artisan migrate:refresh
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SysCategory;

class CreateSysCategoryTable extends Migration
{
    protected $table = 'sys_category';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' );
                $table->integer( 'iCategoryType' );
                $table->integer( 'iParentId' );
                $table->string( 'vCategoryValue', 255 )->nullable();
                $table->string( 'vCategoryName', 255 )->nullable();
                $table->text( 'vImages' )->nullable();
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->integer( 'iRank' );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );

            $data_arr = [
                [
                    "iId" => 1,
                    "iCategoryType" => 1,
                    "iParentId" => 0,
                    "vCategoryValue" => 0,
                    "vCategoryName" => "商品類別"
                ],
                [
                    "iId" => 10001,
                    "iCategoryType" => 1,
                    "iParentId" => 1,
                    "vCategoryValue" => 'Default',
                    "vCategoryName" => "未分類商品"
                ],
                [
                    "iId" => 2,
                    "iCategoryType" => 2,
                    "iParentId" => 0,
                    "vCategoryValue" => "Pay",
                    "vCategoryName" => "付款方式",
                ],
                [
                    "iId" => 20001,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "信用卡一次性",
                    "vCategoryName" => "智付通-信用卡一次性",
                ],
                [
                    "iId" => 20002,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "信用卡分期-3期",
                    "vCategoryName" => "智付通-信用卡分期-3期",
                ],
                [
                    "iId" => 20003,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "信用卡分期-6期",
                    "vCategoryName" => "智付通-信用卡分期-6期",
                ],
                [
                    "iId" => 20004,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "信用卡分期-12期",
                    "vCategoryName" => "智付通-信用卡分期-12期",
                ],
                [
                    "iId" => 20005,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "信用卡分期-18期",
                    "vCategoryName" => "智付通-信用卡分期-18期",
                ],
                [
                    "iId" => 20006,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "信用卡分期-24期",
                    "vCategoryName" => "智付通-信用卡分期-24期",
                ],
                [
                    "iId" => 20007,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "網路 ATM",
                    "vCategoryName" => "智付通-網路 ATM",
                ],
                [
                    "iId" => 20008,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "ATM 轉帳付款",
                    "vCategoryName" => "智付通-ATM 轉帳付款",
                ],
                [
                    "iId" => 20009,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "超商代碼繳費",
                    "vCategoryName" => "智付通-超商代碼繳費",
                ],
                [
                    "iId" => 20010,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "條碼繳費",
                    "vCategoryName" => "智付通-條碼繳費",
                ],
                [
                    "iId" => 20011,
                    "iCategoryType" => 2,
                    "iParentId" => 2,
                    "vCategoryValue" => "銀聯卡",
                    "vCategoryName" => "智付通-銀聯卡",
                ],
                [
                    "iId" => 3,
                    "iCategoryType" => 3,
                    "iParentId" => 0,
                    "vCategoryValue" => 0,
                    "vCategoryName" => "公告類別"
                ],
                [
                    "iId" => 30001,
                    "iCategoryType" => 3,
                    "iParentId" => 3,
                    "vCategoryValue" => 'Default',
                    "vCategoryName" => "未分類"
                ],
                [
                    "iId" => 4,
                    "iCategoryType" => 4,
                    "iParentId" => 0,
                    "vCategoryValue" => 0,
                    "vCategoryName" => "活動類別"
                ],
                [
                    "iId" => 40001,
                    "iCategoryType" => 4,
                    "iParentId" => 4,
                    "vCategoryValue" => 'Default',
                    "vCategoryName" => "未分類"
                ],
                [
                    "iId" => 5,
                    "iCategoryType" => 5,
                    "iParentId" => 0,
                    "vCategoryValue" => 0,
                    "vCategoryName" => "館別"
                ],
                [
                    "iId" => 50001,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '1',
                    "vCategoryName" => "A01館"
                ],
                [
                    "iId" => 50002,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '2',
                    "vCategoryName" => "A02館"
                ],
                [
                    "iId" => 50003,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '3',
                    "vCategoryName" => "A03館"
                ],
                [
                    "iId" => 50004,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '4',
                    "vCategoryName" => "A04館"
                ],
                [
                    "iId" => 50005,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '5',
                    "vCategoryName" => "A05館"
                ],
                [
                    "iId" => 50006,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '6',
                    "vCategoryName" => "A06館"
                ],
                [
                    "iId" => 50007,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '7',
                    "vCategoryName" => "A07館"
                ],
                [
                    "iId" => 50008,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '8',
                    "vCategoryName" => "A08館"
                ],
                [
                    "iId" => 50009,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '9',
                    "vCategoryName" => "A09館"
                ],
                [
                    "iId" => 50010,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '10',
                    "vCategoryName" => "A10館"
                ],
                [
                    "iId" => 50011,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '11',
                    "vCategoryName" => "A11館"
                ],
                [
                    "iId" => 50012,
                    "iCategoryType" => 5,
                    "iParentId" => 5,
                    "vCategoryValue" => '12',
                    "vCategoryName" => "A12館"
                ],
                [
                    "iId" => 6,
                    "iCategoryType" => 6,
                    "iParentId" => 0,
                    "vCategoryValue" => 0,
                    "vCategoryName" => "職稱/職位"
                ],
                [
                    "iId" => 60001,
                    "iCategoryType" => 6,
                    "iParentId" => 6,
                    "vCategoryValue" => '1',
                    "vCategoryName" => "理事長"
                ],

            ];
            $maxRank = 1;
            foreach ($data_arr as $key => $var) {
                $Dao = new SysCategory ();
                $Dao->iId = $var ['iId'];
                $Dao->iMemberId = 1;
                $Dao->iCategoryType = $var ['iCategoryType'];
                $Dao->iParentId = $var ['iParentId'];
                $Dao->vCategoryValue = $var ['vCategoryValue'];
                $Dao->vCategoryName = $var ['vCategoryName'];
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iStatus = 0;
                $Dao->iRank = $maxRank;
                $Dao->save();
                $maxRank++;
            }
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
