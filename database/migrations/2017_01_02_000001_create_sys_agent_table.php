<?php
// php artisan make:migration create_sys_agent_table
// php artisan migrate
// php artisan migrate:refresh
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SysAgent;

class CreateSysAgentTable extends Migration
{
    protected $table = 'sys_agent';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->string( 'vAgentCode', 20 )->unique();
                $table->string( 'vAgentName', 20 );
                $table->string( 'vAgentTaxID', 20 )->unique();
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
            } );

            $data_arr = [
                [
                    "vAgentCode" => "PTW10001",
                    "vAgentName" => "Pin2wall",
                    "vAgentTaxID" => "24954821"
                ]
            ];
            foreach ($data_arr as $key => $var) {
                $Dao = new SysAgent();
                $Dao->vAgentCode = $var ['vAgentCode'];
                $Dao->vAgentName = $var ['vAgentName'];
                $Dao->vAgentTaxID = $var ['vAgentTaxID'];
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iStatus = 1;
                $Dao->save();
            }
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
