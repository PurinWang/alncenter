<?php
// php artisan make:migration create_sys_member_sign_table
// php artisan migrate
// php artisan migrate:refresh
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysMemberSignTable extends Migration
{
    protected $table = 'sys_member_sign';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' );
                $table->string( 'vSessionId' )->nullable();
                $table->string( 'vUserName' )->nullable();
                $table->integer( 'iSignId' );
                $table->text( 'vSignContent' );
                $table->integer( 'iCreateTime' );
                $table->string( 'vIP' );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
