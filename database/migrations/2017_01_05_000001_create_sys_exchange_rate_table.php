<?php

// php artisan make:migration create_sys_exchange_rate_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysExchangeRateTable extends Migration
{
    protected $table = 'sys_exchange_rate';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->string( 'vRateName', 255 ); //幣別
                $table->double( 'fRateValue' );//匯率
                $table->double( 'fRateValueNow' );//匯率現價
                $table->double( 'fRateValueLow' );//匯率低價
                $table->double( 'fRateValueHigh' );//匯率高價
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
            } );
        } else {
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
