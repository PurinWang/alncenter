<?php
// php artisan make:migration create_mod_museum_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModMuseumTable extends Migration
{
    protected $table = 'mod_museum';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iType' )->default( 1 ); //
                $table->string( 'vMuseumLang', 255 )->nullable();//語言類別
                $table->string( 'vMuseumLogo', 255 )->nullable(); //
                $table->string( 'vMuseumCode', 255 )->nullable(); //
                $table->string( 'vMuseumName', 255 )->nullable(); //
                $table->string( 'vMuseumSummary', 255 )->nullable(); //
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->tinyInteger( 'bShow' )->default( 0 );
                $table->tinyInteger( 'bOpen' )->default( 0 );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
