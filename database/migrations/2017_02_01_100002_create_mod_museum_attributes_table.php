<?php
// php artisan make:migration create_mod_museum_attributes_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModMuseumAttributesTable extends Migration
{
    protected $table = 'mod_museum_attributes';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMuseumId' )->default( 0 );
                $table->integer( 'vAttributeType' ); //0.商品特色 1.商品規格 2.注意事項 3.購物須知
                $table->string( 'vAttributeTitle', 255 );
                $table->longText( 'vAttributeDetail' ); //
                $table->tinyInteger( 'bShow' )->default( 0 );
                $table->integer( 'iUpdateTime' );
            } );
        } else {
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
