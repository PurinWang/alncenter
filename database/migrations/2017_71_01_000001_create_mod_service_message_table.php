<?php
// php artisan make:migration create_mod_service_message_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModServiceMessageTable extends Migration
{
    protected $table = 'mod_service_message';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->string( 'vSessionId' )->nullable();
                $table->integer( 'iMemberId' )->nullable();
                $table->string( 'vServiceNum' )->nullable(); //1:客服單號
                $table->integer( 'iType' )->default( 0 ); //1:留言
                $table->integer( 'iParentId' )->nullable();
                $table->string( 'vTitle', 255 )->nullable();
                $table->string( 'vSummary', 255 )->nullable();
                $table->string( 'vName', 255 )->nullable();
                $table->string( 'vGender', 255 )->nullable();
                $table->string( 'vEmail', 255 )->nullable();
                $table->string( 'vContact', 255 )->nullable();
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->integer( 'iStatus' )->default( 0 );
                $table->integer( 'bDel' )->default( 0 );

            } );
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
