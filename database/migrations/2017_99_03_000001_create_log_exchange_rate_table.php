<?php
// php artisan make:migration create_log_exchange_rate_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogExchangeRateTable extends Migration
{
    protected $table = 'log_exchange_rate';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->string( 'vRateName', 255 ); //幣別
                $table->double( 'fBuyCash' );//現金買入
                $table->double( 'fSellCash' );//現金賣出
                $table->double( 'fBuySpot' );//即期買入
                $table->double( 'fSellSpot' );//即期賣出
                $table->integer( 'iDateTime' );
                $table->integer( 'iStatus' )->default( 0 );
            } );
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
