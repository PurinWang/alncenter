<?php

// php artisan make:migration create_mod_invest_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModInvestTable extends Migration
{
    protected $table = 'mod_invest';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' );
                $table->string( 'vInvestNum', 255 )->nullable();
                $table->string( 'vType', 255 )->nullable();
                $table->double( 'iCount',4 )->default( 0 );
                $table->double( 'fExchangeRate', 4 )->default( 0 );
                $table->integer( 'iTotal' )->default( 0 );
                $table->integer( 'iPayStatus' )->default( 0 );//0:未付款 1:已付款 2:已取消授權 3:已請款 4:已退款 11:VACC-已取號 99:付款失敗
                $table->string( 'vImages', 255 )->nullable();
                $table->integer( 'iStatus' )->default( 0 );
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
            } );
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
