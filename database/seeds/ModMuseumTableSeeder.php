<?php
// php artisan db:seed
// php artisan db:seed --class ModMuseumTableSeeder

use Illuminate\Database\Seeder;
use App\SysMenu;

class ModMuseumTableSeeder extends Seeder
{
    protected $table = 'mod_museum';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "iType" => 201,
                "vMuseumLogo" => "",
                "vMuseumName" => "A01",
                "vMuseumCode" => "A01",

            ],
            [
                "iType" => 202,
                "vMuseumLogo" => "",
                "vMuseumName" => "A02",
                "vMuseumCode" => "A02",
            ],
            [
                "iType" => 203,
                "vMuseumLogo" => "",
                "vMuseumName" => "A03",
                "vMuseumCode" => "A03",
            ],
            [
                "iType" => 204,
                "vMuseumLogo" => "",
                "vMuseumName" => "A04",
                "vMuseumCode" => "A04",
            ],
            [
                "iType" => 205,
                "vMuseumLogo" => "",
                "vMuseumName" => "A05",
                "vMuseumCode" => "A05",
            ],
            [
                "iType" => 206,
                "vMuseumLogo" => "",
                "vMuseumName" => "A06",
                "vMuseumCode" => "A06",
            ],
            [
                "iType" => 207,
                "vMuseumLogo" => "",
                "vMuseumName" => "A07",
                "vMuseumCode" => "A07",
            ],
            [
                "iType" => 208,
                "vMuseumLogo" => "",
                "vMuseumName" => "A08",
                "vMuseumCode" => "A08",
            ],
            [
                "iType" => 209,
                "vMuseumLogo" => "",
                "vMuseumName" => "A09",
                "vMuseumCode" => "A09",
            ],
            [
                "iType" => 210,
                "vMuseumLogo" => "",
                "vMuseumName" => "A10",
                "vMuseumCode" => "A10",
            ],
            [
                "iType" => 211,
                "vMuseumLogo" => "",
                "vMuseumName" => "A11",
                "vMuseumCode" => "A11",
            ],
            [
                "iType" => 212,
                "vMuseumLogo" => "",
                "vMuseumName" => "A12",
                "vMuseumCode" => "A12",
            ],
        ];
        foreach ($data_arr as $key => $var) {
            //
            $Dao = new \App\ModMuseum();
            $Dao->iType = $var ['iType'];
            $Dao->vMuseumLogo = $var ['vMuseumLogo'];
            $Dao->vMuseumName = $var ['vMuseumName'];
            $Dao->vMuseumCode = $var ['vMuseumCode'];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->iStatus = 1;
            $Dao->save();
        }
    }
}
