<?php
// php artisan db:seed
// php artisan db:seed --class SysConfigTableSeeder

use Illuminate\Database\Seeder;
use App\SysMenu;

class SysConfigTableSeeder extends Seeder
{
    protected $table = 'sys_config';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "iType" => 171,
                "vTitle" => "網站名稱",
                "vField" => "vTitle",
                "vValue" => "ALLN",
            ],
            [
                "iType" => 171,
                "vTitle" => "網站簡介",
                "vField" => "vDescription",
                "vValue" => "ALLN",
            ],
            [
                "iType" => 172,
                "vTitle" => "網站LOGO",
                "vField" => "vLogo",
                "vValue" => asset( "/images/logo.png" ),
            ],
            [
                "iType" => 172,
                "vTitle" => "網站圖片",
                "vField" => "vImage",
                "vValue" => asset( "/images/logo.png" ),
            ],
            [
                "iType" => 173,
                "vTitle" => "GTM",
                "vField" => "vGTM",
                "vValue" => "GTM-XXXXX",
            ],
            [
                "iType" => 173,
                "vTitle" => "GA",
                "vField" => "vGA",
                "vValue" => "UA-XXXXXXX-1",
            ],
            [
                "iType" => 174,
                "vTitle" => "FB",
                "vField" => "vFB",
                "vValue" => "",
            ],
            [
                "iType" => 174,
                "vTitle" => "Line",
                "vField" => "vLine",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "1",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "2",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "3",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "4",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "5",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "6",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "7",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 601,
                "vTitle" => "8",
                "vField" => "vUrl",
                "vValue" => "",
            ],
            [
                "iType" => 602,
                "vTitle" => "timer",
                "vField" => "iDateTime",
                "vValue" => "",
            ],
            [
                "iType" => 603,
                "vTitle" => "ALN",
                "vField" => "vAlnCount",
                "vValue" => "227500000",
            ],
            [
                "iType" => 603,
                "vTitle" => "BeginTime",
                "vField" => "iStartTime",
                "vValue" => "",
            ],
            [
                "iType" => 603,
                "vTitle" => "EndTime",
                "vField" => "iEndTime",
                "vValue" => "",
            ],
            [
                "iType" => 603,
                "vTitle" => "SoftLimit",
                "vField" => "iSoftLimit",
                "vValue" => "0",
            ],
            [
                "iType" => 603,
                "vTitle" => "Process",
                "vField" => "vProcess",
                "vValue" => "0",
            ],
        ];
        foreach ($data_arr as $key => $var) {
            $Dao = new \App\SysConfig();
            $Dao->iType = $var ['iType'];
            $Dao->vTitle = $var ['vTitle'];
            $Dao->vField = $var ['vField'];
            $Dao->vValue = $var ['vValue'];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->iStatus = 1;
            $Dao->save();
        }
    }
}
