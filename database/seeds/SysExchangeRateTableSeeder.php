<?php
// php artisan db:seed
// php artisan db:seed --class SysExchangeRateTableSeeder

use Illuminate\Database\Seeder;
use App\SysMenu;

class SysExchangeRateTableSeeder extends Seeder
{
    protected $table = 'sys_exchange_rate';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "vRateName" => "ETH",
                "fRateValue" => "500",
                "fRateValueNow" => "558",
                "fRateValueLow" => "515",
                "fRateValueHigh" => "567",
            ]
        ];
        foreach ($data_arr as $key => $var) {
            //
            $Dao = new \App\SysExchangeRate();
            $Dao->vRateName = $var ['vRateName'];
            $Dao->fRateValue = $var ['fRateValue'];
            $Dao->fRateValueNow = $var ['fRateValueNow'];
            $Dao->fRateValueLow = $var ['fRateValueLow'];
            $Dao->fRateValueHigh = $var ['fRateValueHigh'];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->save();
        }
    }
}
