<?php
// php artisan db:seed
// php artisan db:seed --class SysGroupMemberTableSeeder

use Illuminate\Database\Seeder;

class SysGroupMemberTableSeeder extends Seeder
{
    protected $table = 'sys_group_member';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "iGroupId" => 1,
                "iMemberId" => 1
            ],
            [
                "iGroupId" => 2,
                "iMemberId" => 1
            ],
            [
                "iGroupId" => 3,
                "iMemberId" => 1
            ],
            [
                "iGroupId" => 4,
                "iMemberId" => 1
            ]
        ];
        foreach ($data_arr as $key => $var) {
            $Dao = new \App\SysGroupMember();
            $Dao->iGroupId = $var ['iGroupId'];
            $Dao->iMemberId = $var ['iMemberId'];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->iStatus = 1;
            $Dao->save();
        }

        for ($i = 2 ; $i < 17 ; $i++) {
            $Dao = new \App\SysGroupMember();
            $Dao->iGroupId = 1;
            $Dao->iMemberId = $i;
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->iStatus = 1;
            $Dao->save();
        }
    }
}
