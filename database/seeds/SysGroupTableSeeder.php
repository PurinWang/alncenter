<?php
// php artisan db:seed
// php artisan db:seed --class SysGroupTableSeeder

use Illuminate\Database\Seeder;

class SysGroupTableSeeder extends Seeder
{
    protected $table = 'sys_group';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "iId" => 1,
                "iMemberId" => 1,
                "iManagerId" => 1,
                "iGroupType" => 3, //部門
                "vGroupCode" => "ALLN",
                "vGroupName" => "ALLN部門",
                "iLimitCount" => 0
            ],
            [
                "iId" => 5,
                "iMemberId" => 1,
                "iManagerId" => 1,
                "iGroupType" => 999, //一般會員
                "vGroupCode" => "ALLN",
                "vGroupName" => "ALLN KYC",
                "iLimitCount" => 0
            ],
        ];
        foreach ($data_arr as $key => $var) {
            $Dao = new \App\SysGroup();
            $Dao->iId = $var ['iId'];
            $Dao->iMemberId = $var ['iMemberId'];
            $Dao->iManagerId = $var ['iManagerId'];
            $Dao->iGroupType = $var ['iGroupType'];
            $Dao->vGroupCode = $var ['vGroupCode'];
            $Dao->vGroupName = $var ['vGroupName'];
            $Dao->iLimitCount = $var ['iLimitCount'];
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->bPublic = 0;
            $Dao->iStatus = 1;
            $Dao->bDel = 0;
            $Dao->save();
        }
    }
}
