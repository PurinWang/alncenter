<?php
// php artisan db:seed
// php artisan db:seed --class SysMemberInfoTableSeeder

use Illuminate\Database\Seeder;

class SysMemberInfoTableSeeder extends Seeder
{
    protected $table = 'sys_member_info';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "vUserImage" => "/images/admin.jpg",
                "vUserName" => "Admin",
                "vUserEmail" => "elvis@pin2wall.com",
                "vUserContact" => "0911759743"
            ],
            [
                "vUserImage" => "/images/manager.jpg",
                "vUserName" => "Manager",
                "vUserEmail" => "elvis@pin2wall.com",
                "vUserContact" => "0911759743"
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnAdmin1",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnAdmin2",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnAdmin3",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnAdmin4",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnAdmin5",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnManage1",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnManage2",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnManage3",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnManage4",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnManage5",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnSupport1",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnSupport2",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnSupport3",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnSupport4",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],
            [
                "vUserImage" => "/images/alln.jpg",
                "vUserName" => "AllnSupport5",
                "vUserEmail" => "",
                "vUserContact" => ""
            ],

        ];
        $iMemberId = 1;
        foreach ($data_arr as $key => $var) {
            $Dao = new \App\SysMemberInfo();
            $Dao->iMemberId = $iMemberId;
            $Dao->vUserImage = $var ['vUserImage'];
            $Dao->vUserName = $var ['vUserName'];
            $Dao->vUserEmail = $var ['vUserEmail'];
            $Dao->vUserContact = $var ['vUserContact'];
            $Dao->save();
            $iMemberId++;
        }
    }
}
