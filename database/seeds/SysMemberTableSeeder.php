<?php
// php artisan db:seed
// php artisan db:seed --class SysMemberTableSeeder

use Illuminate\Database\Seeder;

class SysMemberTableSeeder extends Seeder
{
    protected $table = 'sys_member';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            [
                "iAcType" => 1,
                "vAccount" => "admin@pin2wall.com",
                "vPassword" => "abc123"
            ],
            [
                "iAcType" => 2,
                "vAccount" => "manager@pin2wall.com",
                "vPassword" => "abc123"
            ],
            [
                "iAcType" => 2,
                "vAccount" => "allnadmin1",
                "vPassword" => "03522003"
            ],
            [
                "iAcType" => 2,
                "vAccount" => "allnadmin2",
                "vPassword" => "03522003"
            ],
            [
                "iAcType" => 2,
                "vAccount" => "allnadmin3",
                "vPassword" => "03522003"
            ],
            [
                "iAcType" => 2,
                "vAccount" => "allnadmin4",
                "vPassword" => "03522003"
            ],
            [
                "iAcType" => 2,
                "vAccount" => "allnadmin5",
                "vPassword" => "03522003"
            ],
            [
                "iAcType" => 11,
                "vAccount" => "allnmanage1",
                "vPassword" => "4499567"
            ],
            [
                "iAcType" => 11,
                "vAccount" => "allnmanage2",
                "vPassword" => "4499567"
            ],
            [
                "iAcType" => 11,
                "vAccount" => "allnmanage3",
                "vPassword" => "4499567"
            ],
            [
                "iAcType" => 11,
                "vAccount" => "allnmanage4",
                "vPassword" => "4499567"
            ],
            [
                "iAcType" => 11,
                "vAccount" => "allnmanage5",
                "vPassword" => "4499567"
            ],
            [
                "iAcType" => 31,
                "vAccount" => "allnsupport1",
                "vPassword" => "allntoken"
            ],
            [
                "iAcType" => 31,
                "vAccount" => "allnsupport2",
                "vPassword" => "allntoken"
            ],
            [
                "iAcType" => 31,
                "vAccount" => "allnsupport3",
                "vPassword" => "allntoken"
            ],
            [
                "iAcType" => 31,
                "vAccount" => "allnsupport4",
                "vPassword" => "allntoken"
            ],
            [
                "iAcType" => 31,
                "vAccount" => "allnsupport5",
                "vPassword" => "allntoken"
            ],


        ];
        $iUserId = 1000000001;
        foreach ($data_arr as $key => $var) {
            $str = md5( uniqid( mt_rand(), true ) );
            $uuid = substr( $str, 0, 8 ) . '-';
            $uuid .= substr( $str, 8, 4 ) . '-';
            $uuid .= substr( $str, 12, 4 ) . '-';
            $uuid .= substr( $str, 16, 4 ) . '-';
            $uuid .= substr( $str, 20, 12 );
            //
            $Dao = new \App\SysMember();
            $Dao->vAgentCode = "PTW10001";
            $Dao->iUserId = $iUserId;
            $Dao->vUserCode = $uuid;
            $Dao->iAcType = $var ['iAcType'];
            $Dao->vAccount = $var ['vAccount'];
            $Dao->vPassword = hash( 'sha256', $Dao->vAgentCode . md5( $var ['vPassword'] ) . $Dao->vUserCode );
            $Dao->vCreateIP = "";
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->bActive = 1;
            $Dao->iStatus = 1;
            $Dao->save();
            $iUserId++;
        }
    }
}
