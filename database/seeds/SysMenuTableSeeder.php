<?php
// php artisan db:seed
// php artisan db:seed --class SysMenuTableSeeder

use Illuminate\Database\Seeder;
use App\SysMenu;

class SysMenuTableSeeder extends Seeder
{
    protected $table = 'sys_menu';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table( $this->table )->delete();
        //
        $data_arr = [
            // admin
            [
                //管理員功能
                "iId" => 1,
                "vName" => "admin",
                "vUrl" => "",
                "vCss" => "fa-power-off",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //帳號管理
                            "iId" => 11,
                            "vName" => "admin.member",
                            "vUrl" => "",
                            "vCss" => "",
                            "bSubMenu" => 1,
                            "iParentId" => 1,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                            "child" =>
                                [
                                    [
                                        //一般會員管理
                                        "iId" => 111,
                                        "vName" => "admin.member.customer",
                                        "vUrl" => "web/admin/member/customer",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 11,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //部門成員管理
                                        "iId" => 112,
                                        "vName" => "admin.member.employee",
                                        "vUrl" => "web/admin/member/employee",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 11,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ]
                                ]
                        ],
                        [
                            //群組管理
                            "iId" => 12,
                            "vName" => "admin.group",
                            "vUrl" => "",
                            "vCss" => "",
                            "bSubMenu" => 1,
                            "iParentId" => 1,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                            "child" =>
                                [
                                    [
                                        //一般會員管理
                                        "iId" => 121,
                                        "vName" => "admin.group.customer",
                                        "vUrl" => "web/admin/group/customer",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 12,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //部門成員管理
                                        "iId" => 122,
                                        "vName" => "admin.group.employee",
                                        "vUrl" => "web/admin/group/employee",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 12,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ]
                                ]
                        ],
                        [
                            //匯率管理
                            "iId" => 13,
                            "vName" => "admin.exchange_rate",
                            "vUrl" => "",
                            "vCss" => "",
                            "bSubMenu" => 1,
                            "iParentId" => 1,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                            "child" =>
                                [
                                    [
                                        //匯率設定
                                        "iId" => 131,
                                        "vName" => "admin.exchange_rate.index",
                                        "vUrl" => "web/admin/exchange_rate/index",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 13,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //歷史匯率
                                        "iId" => 132,
                                        "vName" => "admin.exchange_rate.log",
                                        "vUrl" => "web/admin/exchange_rate/log",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 13,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                ]
                        ],
                        [
                            //系統類別設置
                            "iId" => 14,
                            "vName" => "admin.category",
                            "vUrl" => "web/admin/category",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 1,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //系統參數設置
                            "iId" => 17,
                            "vName" => "admin.config",
                            "vUrl" => "web/admin/config",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 1,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                    ],
            ],
            //
            [
                //分會分館館理
                "iId" => 2,
                "vName" => "museum",
                "vUrl" => "",
                "vCss" => "fa-cubes",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //A01
                            "iId" => 201,
                            "vName" => "museum.a01",
                            "vUrl" => "web/museum/a01",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A02
                            "iId" => 202,
                            "vName" => "museum.a02",
                            "vUrl" => "web/museum/a02",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A03
                            "iId" => 203,
                            "vName" => "museum.a03",
                            "vUrl" => "web/museum/a03",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A04
                            "iId" => 204,
                            "vName" => "museum.a04",
                            "vUrl" => "web/museum/a04",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A05
                            "iId" => 205,
                            "vName" => "museum.a05",
                            "vUrl" => "web/museum/a05",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A06
                            "iId" => 206,
                            "vName" => "museum.a06",
                            "vUrl" => "web/museum/a06",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A07
                            "iId" => 207,
                            "vName" => "museum.a07",
                            "vUrl" => "web/museum/a07",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A08
                            "iId" => 208,
                            "vName" => "museum.a08",
                            "vUrl" => "web/museum/a08",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,",
                            "bOpen" => 1,
                        ],
                        [
                            //A09
                            "iId" => 209,
                            "vName" => "museum.a09",
                            "vUrl" => "web/museum/a09",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A10
                            "iId" => 210,
                            "vName" => "museum.a10",
                            "vUrl" => "web/museum/a10",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A11
                            "iId" => 211,
                            "vName" => "museum.a11",
                            "vUrl" => "web/museum/a11",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //A12
                            "iId" => 212,
                            "vName" => "museum.a12",
                            "vUrl" => "web/museum/a12",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 2,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                    ]
            ],
            //
            [
                //首頁資訊管理
                "iId" => 6,
                "vName" => "home",
                "vUrl" => "",
                "vCss" => "fa-cubes",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //white_paper
                            "iId" => 601,
                            "vName" => "home.white_paper",
                            "vUrl" => "web/home/white_paper",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 6,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //活動計時器
                            "iId" => 602,
                            "vName" => "home.activity",
                            "vUrl" => "web/home/activity",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 6,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //ICO目標
                            "iId" => 603,
                            "vName" => "home.milestone",
                            "vUrl" => "web/home/milestone",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 6,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                        [
                            //light_paper
                            "iId" => 604,
                            "vName" => "home.light_paper",
                            "vUrl" => "web/home/light_paper",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 6,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                    ]
            ],
            //
            [
                //Invest
                "iId" => 7,
                "vName" => "invest",
                "vUrl" => "",
                "vCss" => "fa-cubes",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2,11",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //index
                            "iId" => 701,
                            "vName" => "invest.index",
                            "vUrl" => "web/invest/index",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 7,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                        [
                            //bonus
                            "iId" => 702,
                            "vName" => "invest.bonus",
                            "vUrl" => "web/invest/bonus",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 7,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                        [
                            //index
                            "iId" => 703,
                            "vName" => "invest.index_1",
                            "vUrl" => "web/invest/index_1",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 7,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                        [
                            //index
                            "iId" => 704,
                            "vName" => "invest.index_2",
                            "vUrl" => "web/invest/index_2",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 7,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                        [
                            //index
                            "iId" => 705,
                            "vName" => "invest.index_3",
                            "vUrl" => "web/invest/index_3",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 7,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                    ]
            ],
            [
                //Coin Airdrops
                "iId" => 8,
                "vName" => "coin_airdrops",
                "vUrl" => "",
                "vCss" => "fa-cubes",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2,11",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //index
                            "iId" => 801,
                            "vName" => "coin_airdrops.index",
                            "vUrl" => "web/coin_airdrops/index",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 8,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                        [
                            //user
                            "iId" => 802,
                            "vName" => "coin_airdrops.user",
                            "vUrl" => "web/coin_airdrops/user",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 8,
                            "vAccess" => "1,2,11",
                            "bOpen" => 1,
                        ],
                    ]
            ],
            [
                //訊息公告
                "iId" => 4001,
                "vName" => "news",
                "vUrl" => "",
                "vCss" => "fa-list",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2,11,31",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //公告訊息
                            "iId" => 40011,
                            "vName" => "news.index",
                            "vUrl" => "web/news/index",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 4001,
                            "vAccess" => "1,2,11,31",
                            "bOpen" => 1,
                        ],
                    ]
            ],
            [
                //客服專區
                "iId" => 5001,
                "vName" => "service",
                "vUrl" => "",
                "vCss" => "fa-list",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2,11,31",
                "bOpen" => 1,
                "child" =>
                    [
                        [
                            //訊息
                            "iId" => 50011,
                            "vName" => "service.message",
                            "vUrl" => "web/service/message",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 5001,
                            "vAccess" => "1,2,11,31",
                            "bOpen" => 1,
                        ],
                        [
                            //訊息
                            "iId" => 50012,
                            "vName" => "service.email",
                            "vUrl" => "web/service/email",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 5001,
                            "vAccess" => "1,2,11,31",
                            "bOpen" => 1,
                        ],
                    ]
            ],
            [
                //
                "iId" => 99001,
                "vName" => "log",
                "vUrl" => "",
                "vCss" => "fa-list",
                "bSubMenu" => 1,
                "iParentId" => 0,
                "vAccess" => "1,2",
                "bOpen" => 0,
                "child" =>
                    [
                        [
                            //
                            "iId" => 9900101,
                            "vName" => "log.log01",
                            "vUrl" => "web/log/log01",
                            "vCss" => "",
                            "bSubMenu" => 0,
                            "iParentId" => 99001,
                            "vAccess" => "1,2",
                            "bOpen" => 1,
                        ],
                    ]
            ],
        ];

        $maxRank = 0;
        foreach ($data_arr as $key => $var) {
            $Dao = new SysMenu();
            $Dao->iId = $var ['iId'];
            $Dao->iType = isset( $var ['iType'] ) ? $var ['iType'] : 0;
            $Dao->vName = $var ['vName'];
            $Dao->vUrl = $var ['vUrl'];
            $Dao->vCss = $var ['vCss'];
            $Dao->bSubMenu = $var ['bSubMenu'];
            $Dao->iParentId = $var ['iParentId'];
            $Dao->vAccess = $var ['vAccess'];
            $Dao->bOpen = $var ['bOpen'];
            $Dao->iRank = ( isset( $var ['iRank'] ) ) ? $var ['iRank'] : $maxRank + 1;
            $maxRank++;
            $Dao->save();
            if ($Dao->bSubMenu) {
                $maxRank_child = 0;
                foreach ($var['child'] as $key_child => $var_child) {
                    $DaoChild = new SysMenu();
                    $DaoChild->iId = $var_child ['iId'];
                    $DaoChild->iType = isset( $var_child ['iType'] ) ? $var_child ['iType'] : 0;
                    $DaoChild->vName = $var_child ['vName'];
                    $DaoChild->vUrl = $var_child ['vUrl'];
                    $DaoChild->vCss = $var_child ['vCss'];
                    $DaoChild->bSubMenu = $var_child ['bSubMenu'];
                    $DaoChild->iParentId = $var_child ['iParentId'];
                    $DaoChild->vAccess = $var_child ['vAccess'];
                    $DaoChild->bOpen = $var_child ['bOpen'];
                    $DaoChild->iRank = ( isset( $var_child ['iRank'] ) ) ? $var_child ['iRank'] : $maxRank_child + 1;
                    $DaoChild->save();
                    $maxRank_child++;
                    if ($DaoChild->bSubMenu) {
                        $maxRank_child2 = 0;
                        foreach ($var_child['child'] as $key_child2 => $var_child2) {
                            $DaoChild2 = new SysMenu ();
                            $DaoChild2->iId = $var_child2 ['iId'];
                            $DaoChild2->iType = isset( $var_child2 ['iType'] ) ? $var_child2 ['iType'] : 0;
                            $DaoChild2->vName = $var_child2 ['vName'];
                            $DaoChild2->vUrl = $var_child2 ['vUrl'];
                            $DaoChild2->vCss = $var_child2 ['vCss'];
                            $DaoChild2->bSubMenu = $var_child2 ['bSubMenu'];
                            $DaoChild2->iParentId = $var_child2 ['iParentId'];
                            $DaoChild2->vAccess = $var_child2 ['vAccess'];
                            $DaoChild2->bOpen = $var_child2 ['bOpen'];
                            $DaoChild2->iRank = ( isset( $var_child2 ['iRank'] ) ) ? $var_child2 ['iRank'] : $maxRank_child2 + 1;
                            $DaoChild2->save();
                            $maxRank_child2++;
                        }
                    }
                }
            }
        }
    }
}
