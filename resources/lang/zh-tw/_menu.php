<?php
return [
    'member' =>
        [
            'title' => '會員',
            'userinfo' =>
                [
                    'title' => '個人資料',
                ]
        ],
    'admin' =>
        [
            'title' => '管理員功能',
            'member' =>
                [
                    'title' => '帳號管理',
                    'customer' => [
                        'title' => '一般會員帳號',
                        'add' =>
                            [
                                'title' => '新增一般會員'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ],
                        'access' =>
                            [
                                'title' => '權限設置',
                            ],
                    ],
                    'employee' => [
                        'title' => '部門員工帳號',
                        'add' =>
                            [
                                'title' => '新增員工'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ],
                        'access' =>
                            [
                                'title' => '權限設置',
                            ],
                    ],
                    'store' => [
                        'title' => '自營店家帳號',
                        'add' =>
                            [
                                'title' => '新增店家'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ],
                        'access' =>
                            [
                                'title' => '權限設置',
                            ],
                    ],
                    'blogger' => [
                        'title' => '合作廠商帳號',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ],
                        'access' =>
                            [
                                'title' => '權限設置',
                            ],
                    ],
                    'supplier' => [
                        'title' => '供應商帳號',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ],
                        'access' =>
                            [
                                'title' => '權限設置',
                            ],
                    ],
                ],
            'group' =>
                [
                    'title' => '群組設置',
                    'customer' => [
                        'title' => '一般會員群組',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ]
                    ],
                    'employee' => [
                        'title' => '部門群組',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ]
                    ],
                    'store' => [
                        'title' => '店家群組',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ]
                    ],
                    'blogger' => [
                        'title' => '合作廠商群組',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ]
                    ],
                    'supplier' => [
                        'title' => '供應商群組',
                        'add' =>
                            [
                                'title' => '新增'
                            ],
                        'edit' =>
                            [
                                'title' => '編輯'
                            ]
                    ],
                ],
            'category' =>
                [
                    'title' => '系統類別管理',
                    'sub' =>
                        [
                            'title' => '類別項目'
                        ]
                ],
            'exchange_rate' =>
                [
                    'title' => '匯率管理',
                    'index' => [
                        'title' => '匯率設定',
                    ],
                    'log' => [
                        'title' => '歷史匯率',
                    ],
                ],
            'config' =>
                [
                    'title' => '系統參數管理',
                ],
        ],
    'museum' =>
        [
            'title' => '語系頁面管理',
            'a01' =>
                [
                    'title' => 'A01',
                    'index' =>
                        [
                            'title' => 'A01',
                        ]
                ],
            'a02' =>
                [
                    'title' => 'A02',
                    'index' =>
                        [
                            'title' => 'A02',
                        ]
                ],
            'a03' =>
                [
                    'title' => 'A03',
                    'index' =>
                        [
                            'title' => 'A03',
                        ]
                ],
            'a04' =>
                [
                    'title' => 'A04',
                    'index' =>
                        [
                            'title' => 'A04',
                        ]
                ],
            'a05' =>
                [
                    'title' => 'A05',
                    'index' =>
                        [
                            'title' => 'A05',
                        ]
                ],
            'a06' =>
                [
                    'title' => 'A06',
                    'index' =>
                        [
                            'title' => 'A06',
                        ]
                ],
            'a07' =>
                [
                    'title' => 'A07',
                    'index' =>
                        [
                            'title' => 'A07',
                        ]
                ],
            'a08' =>
                [
                    'title' => 'A08',
                    'index' =>
                        [
                            'title' => 'A08',
                        ]
                ],
            'a09' =>
                [
                    'title' => 'A09',
                    'index' =>
                        [
                            'title' => 'A09',
                        ]
                ],
            'a10' =>
                [
                    'title' => 'A10',
                    'index' =>
                        [
                            'title' => 'A10',
                        ]
                ],
            'a11' =>
                [
                    'title' => 'A11',
                    'index' =>
                        [
                            'title' => 'A11',
                        ]
                ],
            'a12' =>
                [
                    'title' => 'A12',
                    'index' =>
                        [
                            'title' => 'A12',
                        ]
                ]
        ],
    'home' =>
        [
            'title' => '首頁資訊管理',
            'white_paper' =>
                [
                    'title' => '白皮書下載點',
                ],
            'activity' =>
                [
                    'title' => '活動計時器',
                ],
            'milestone' =>
                [
                    'title' => 'ICO目標',
                ],
            'light_paper' =>
                [
                    'title' => 'Light Paper',
                ],
        ],
    'invest' =>
        [
            'title' => 'ICO投資',
            'index' =>
                [
                    'title' => '投資意願單列表',
                ],
            'bonus' =>
                [
                    'title' => 'Bonus',
                ],
            'index_1' =>
                [
                    'title' => 'ETH',
                ],
            'index_2' =>
                [
                    'title' => 'BTC',
                ],
            'index_3' =>
                [
                    'title' => 'USD',
                ],
        ],
    'coin_airdrops' =>
        [
            'title' => '空投任務',
            'index' =>
                [
                    'title' => '任務管理',
                ],
            'user' =>
                [
                    'title' => '空投名單',
                ],
        ],
    'news' =>
        [
            'title' => '訊息公告',
            'index' =>
                [
                    'title' => '資訊專區',
                ],
        ],
    'service' =>
        [
            'title' => '客服專區',
            'message' =>
                [
                    'title' => '留言專區'
                ],
            'email' =>
                [
                    'title' => '白名單管理'
                ],
        ],
    'log' =>
        [
            'title' => '數據統計與行為記錄',
            'log01' =>
                [
                    'title' => '店家使用記錄'
                ],
        ],
];
