<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
</head>
<div style="margin:0px">
    <div style="font-family:'\005fae\008edf\006b63\009ed1\009ad4','Microsoft JhengHei',Arial,sans-serif;width:100%;background-color:#f3f2ee">
        <table style="width:80%;margin:0px auto;height:120px;background-color:#000;">
            <tbody>
            <tr>
                <td style="width:46%;vertical-align:bottom;padding:8px">
                    <img src="{{asset('images/logo.png')}}" alt="" style="width:170px;height:auto;" class="CToWUd">
                </td>
            </tr>
            </tbody>
        </table>
        <div style="width:80%;margin:0 auto;background-color:#ffffff">
            <div style="color:#6a6a6a;padding:90px 50px;margin-bottom: 20px;">
                <div style="border-left:5px solid #6a8e24;padding:2px 15px;font-size:22px;font-weight:900;letter-spacing:1px;color:#6a6a6a;margin-bottom:50px">
                    <div style="margin-bottom:4px">From：<span style="color:#789162;margin:0 5px;font-weight:800"></span>ALLN-Token Team</div>
                    <div style="font-size:14px;color:#898989;font-weight:normal">Feedback reply / 反馈回覆</div>
                </div>
                <div style="font-size:18px;line-height:30px;padding:0 25px;text-align:justify">
                    <div style="font-size: 22px;margin-bottom: 50px;line-height: 40px;padding: 0 25px;text-align: justify;">
                        <div style="">Title/主题 : {{$info->vTitle}}</div>
                        <div style="">Content/内容 : {{$info->vSummary}}</div>
                        <div style="">Responders/回覆者 : {{$info->vName}}</div>
                    </div>
                </div>
                <div style="font-size:18px;line-height:30px;padding:0 25px;text-align:justify">
                    <br>ALLN-Token Team<br><br>
                    <div style="font-size:14px;line-height:30px;padding:25px 25px 5px;">
                        Facebook：https://www.facebook.com/ALLNTOKEN<br>Twitter：https://t.me/allntoken<br>Email：service@allntoken.io
                    </div>
                </div>
            </div>
            <div style="padding:25px;text-align:center;background-color:#789162;color:#fff;letter-spacing:1px;font-size:16px"></div>
            <div class="yj6qo"></div>
            <div class="adL">
            </div>
        </div>
        <div class="adL">
        </div>
    </div>
</div>
</html>