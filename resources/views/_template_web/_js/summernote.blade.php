<!-- SUMMERNOTE -->
<style>
    <!--
    .popover {
        z-index: 1050;
    }

    -->
</style>
<script src="/web_assets/v3/js/plugin/summernote/summernote.min.js"></script>
<script>
    $(document).ready(function () {
        $('body').on('hidden.bs.modal', function (e) {
            if($('.modal').hasClass('in')) {
                $('body').addClass('modal-open');
            }
        });
        $('.summernote').summernote({
            dialogsInBody: true,
            toolbar: [
                ["style", ["style", "undo", "redo"]],
                ["font", ["bold", "underline", "clear"]],
                ["fontname", ["fontname"]],
                ['fontsize', ['fontsize']],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["table", ["table"]],
                ["insert", ["link", "picture", "video"]],
                ["view", ["fullscreen", "codeview", "help"]]
            ],
            height: 250,
            callbacks: {
                onImageUpload: function (files, editor, welEditable) {
                    sendFile(files[0], this);
                }
            }
        });

        $('.summernote_text').summernote({
            toolbar: [
                // ["style", ["style", "undo", "redo"]],
                 ["font", ["bold", "underline","strikethrough"]],
                // ["fontname", ["fontname"]],
                 ['fontsize', ['fontsize']],
                 ["color", ["color"]],
                // ["para", ["ul", "ol", "paragraph"]],
                // ["table", ["table"]],
                // ["insert", ["link", "picture", "video"]],
                // ["view", ["fullscreen", "codeview", "help"]]
            ],
            height: 250,
            callbacks: {
                onImageUpload: function (files, editor, welEditable) {
                    sendFile(files[0], this);
                }
            }
        });
        $('.note-image-dialog, .note-link-dialog, .note-video-dialog, .note-help-dialog').on('show.bs.modal', function() {
            $(this).detach().appendTo('body');
        });

        $('.note-image-dialog, .note-link-dialog, .note-video-dialog, .note-help-dialog').on('hide.bs.modal', function() {
            $(this).detach().appendTo('.note-dialog');
        });
    });
    function sendFile(files, editor) {
        if (files.size > 2 * 1024 * 1024) {
            swal("{{trans('_web_alert.notice')}}", "{{trans('_web_alert.cropper_image_too_big')}}:2 MB", "error");
            return;
        }
        data = new FormData();
        data.append("_token", "{{ csrf_token() }}");
        data.append("files", files);
        $.ajax({
            data: data,
            type: "POST",
            url: "{{url('web/upload_image')}}",
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $(editor).summernote('editor.insertImage', data.files[0].url);
            }
        });
    }
</script>