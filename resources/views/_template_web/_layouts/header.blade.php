<header id="header" style="background:#333!important">
    <div id="logo-group" style="margin-top:-5px">
        <!-- PLACE YOUR LOGO HERE -->
        <a href="{{url('web')}}">
            <span id="logo"> <img src="/images/logo.png" alt=""></span>
        </a>
    </div>
    <!-- pulled right: nav area -->
    <div class="pull-right">
        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
			<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a>
			</span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
            <li class=""><a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown"> <img src="/images/admin.jpg" alt="John Doe" class="online"/>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="{{url('web/logout')}}" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"
                           data-logout-msg="{{trans('_web_alert.logout.title')}}"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a></li>
                </ul>
            </li>
        </ul>
        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
			<span> <a href="{{url('web/logout')}}" title="Sign Out" data-action="userLogout" data-logout-msg="{{trans('_web_alert.logout.title')}}"><i
                            class="fa fa-sign-out"></i></a>
			</span>
        </div>
        <!-- end logout button -->

        <!-- multiple lang dropdown : find all flags in the flags page -->
        <ul class="header-dropdown-list hidden-xs">
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    @if(Session::get('locale') =='zh-tw')
                        <img src="/web_assets/v3/img/blank.gif" class="flag flag-tw" alt="Taiwan"> <span>繁體中文(TW)</span>
                    @else
                        <img src="/web_assets/v3/img/blank.gif" class="flag flag-us" alt="United States"> <span>English(US)</span>
                    @endif
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li class="@if(Session::get('locale') =='zh-tw') active @endif"><a href="javascript:void(0);" class="btn-locale" data-locale="zh-tw"><img
                                    src="/web_assets/v3/img/blank.gif" class="flag flag-tw" alt="Taiwan"> 繁體中文 (TW)</a></li>
                    <li class="@if(Session::get('locale') =='en') active @endif"><a href="javascript:void(0);" class="btn-locale" data-locale="en"><img
                                    src="/web_assets/v3/img/blank.gif" class="flag flag-us" alt="United States"> English (US)</a></li>
                </ul>
            </li>
        </ul>
        <!-- end multiple lang -->
    </div>
    <!-- end pulled right: nav area -->
</header>