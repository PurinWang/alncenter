<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <title>{{config('_website.web_title')}}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support  -->
    <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/smartadmin-rtl.min.css">

    <!-- We recommend you use "your_style.css" to override SmartAdmin
                 specific styles this will also ensure you retrain your customization with each SmartAdmin update.
            <link rel="stylesheet" type="text/css" media="screen" href="/web_assets/v3/css/your_style.css"> -->
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip
                 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="/web_assets/v3/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/web_assets/v3/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/web_assets/v3/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/web_assets/v3/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="/web_assets/v3/img/splash/ipad-landscape.png"
          media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="/web_assets/v3/img/splash/ipad-portrait.png"
          media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="/web_assets/v3/img/splash/iphone.png" media="screen and (max-device-width: 320px)">
    <!-- dataTables -->
    <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="/web_assets/v1/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="/web_assets/v1/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="/css/pin2wall.css" rel="stylesheet">
    <!-- Loading style -->
    <link rel="stylesheet" href="/web_assets/v3/css/font-awesome/css/font-awesome.min.css">
    <link href="/web_assets/v3/css/loadingStyle.css" rel="stylesheet">
    <!-- ================== page-css ================== -->
@yield('page-css')
<!-- ================== /page-css ================== -->
</head>
<body class="desktop-detected pace-done">
<!-- HEADER -->
@include('_template_web._layouts.header')
<!-- END HEADER -->
<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">
    <!-- User info -->
    <div class="login-info">
			<span> <!-- User image size is adjusted inside CSS, it should stay as it -->
                <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                    <img src="{{session('member.info.vUserImage')}}" alt="me" class="online"/>
                    <span> {{session('member.info.vUserName')}} </span>
                    <i class="fa fa-angle-down"></i>
			    </a>
			</span>
    </div>
    <!-- end user info -->
    <!-- NAVIGATION : This navigation is also responsive-->
    <!-- Public nav -->
@include('_template_web._layouts.nav')
<!-- end -->
    <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i></span>
</aside>
<!-- END NAVIGATION -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <!-- RIBBON -->
    <div id="ribbon">
			<span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom"
                      data-original-title="{!!trans('web.resetwidgets')!!}" data-html="true">
                    <i class="fa fa-refresh"></i>
			    </span>
			</span>
        <ol class="breadcrumb">
            @foreach( $breadcrumb as $key => $var)
                <li><a href="{{$var}}">{{trans ( '_menu.'.$key.'.title' )}}</a></li>
                <!--  -->
            @endforeach
        </ol>
    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
@yield('content')
<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- loading -->
<div class="loadingCube">
    <div id="animation_containerL">
        <div id="dom_overlay_containerL"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
        <div id="loadTxt">Loading...</div>
    </div>
</div>
<!-- end loading -->

<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
    Note: These tiles are completely responsive,
    you can add as many as you like
    -->
<div id="shortcut">
    <ul>
        <li>
            <a href="{{url('web/member/userinfo')}}" class="jarvismetro-tile big-cubes bg-color-pinkDark">
                <span class="iconbox"> <i class="fa fa-user fa-4x"></i>
                    <span>個人資料</span>
                </span>
            </a>
        </li>
    </ul>
</div>
<!-- END SHORTCUT AREA -->
<!-- END SHORTCUT AREA -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="/web_assets/v3/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="/web_assets/v3/js/libs/jquery-2.1.1.min.js"><\/script>');
    }
</script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="/web_assets/v3/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="/web_assets/v3/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="/web_assets/v3/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="/web_assets/v3/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="/web_assets/v3/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="/web_assets/v3/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="/web_assets/v3/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="/web_assets/v3/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="/web_assets/v3/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="/web_assets/v3/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="/web_assets/v3/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="/web_assets/v3/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="/web_assets/v3/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="/web_assets/v3/js/plugin/fastclick/fastclick.min.js"></script>

<!-- Loading -->
<script src="/web_assets/v3/js/loading.js"></script>

<!-- MAIN APP JS FILE -->
<script src="/web_assets/v3/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="/web_assets/v3/js/speech/voicecommand.min.js"></script>
<!-- dataTables -->
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<!-- Sweet alert -->
<script src="/web_assets/v1/js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- Toastr script -->
<script src="/web_assets/v1/js/plugins/toastr/toastr.min.js"></script>
<!-- Public commit -->
@include('_template_web._js.var')
<!-- end -->
<!-- Public commit -->
@include('_template_web._js.commit')
<!-- end -->
<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
<!-- ================== page-js ================== -->
@yield('page-js')
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@yield('inline-js')
<!-- ================== /inline-js================== -->
</body>
</html>