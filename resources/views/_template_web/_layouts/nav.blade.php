<?php
$menu_access = Session::get( 'menu_access' );
$menu_parent = Session::get( 'menu_parent' );
$nav_html = "";
$nav_html .= '<nav>';
$nav_html .= '<ul>';
foreach ($sys_menu as $key => $var) {
    if ($var->iType != 0) {
        continue;
    }
    if ( !Session::get( 'access.' . $var->iId )) {
        continue;
    }
    $active = ( $menu_access == $var->iId || $menu_parent [0] == $var->iId ) ? "active" : "";
    $nav_html .= '<li class="' . $active . '">';
    $nav_html .= '<a href="' . ( ( $var->vUrl != "" ) ? url( $var->vUrl ) : "#" ) . '" title="' . trans( '_menu.' . $var->vName . '.title' ) . '"><i class="fa fa-lg fa-fw ' . $var->vCss . '"></i> <span class="menu-item-parent">' . trans( '_menu.' . $var->vName . '.title' ) . '</span></a>';
    if ($var->bSubMenu) {
        $nav_html .= '<ul>';
        foreach ($var->second as $key2 => $var2) {
            if ( !Session::get( 'access.' . $var2->iId )) {
                continue;
            }
            //暫時取消
            if($var2->vUrl == 'web/travel/log'){
                        continue;
                    }
            //    
            $active = ( $menu_access == $var2->iId || $menu_parent [1] == $var2->iId ) ? "active" : "";
            $nav_html .= '<li class="' . $active . '">';
            $nav_html .= '<a href="' . ( ( $var2->vUrl != "" ) ? url( $var2->vUrl ) : "#" ) . '" title="' . trans( '_menu.' . $var2->vName . '.title' ) . '"><i class="fa fa-lg fa-fw ' . $var2->vCss . '"></i><span class="menu-item-parent">' . trans( '_menu.' . $var2->vName . '.title' ) . '</span></a>';
            if ($var2->bSubMenu) {
                $nav_html .= '<ul>';
                foreach ($var2->third as $key3 => $var3) {
                    if ( !Session::get( 'access.' . $var3->iId )) {
                        continue;
                    }
                    $active = ( $menu_access == $var3->iId ) ? "active" : "";
                    $nav_html .= '<li class="' . $active . '">';
                    $nav_html .= '<a href="' . url( $var3->vUrl ) . '" title="' . trans( '_menu.' . $var3->vName . '.title' ) . '"><i class="fa fa-lg fa-fw ' . $var3->vCss . '"></i><span class="menu-item-parent">' . trans( '_menu.' . $var3->vName . '.title' ) . '</span></a>';
                    $nav_html .= '</li>';
                }
                $nav_html .= '</ul>';
            }
            $nav_html .= '</li>';
        }
        $nav_html .= '</ul>';
    }
    $nav_html .= '</li>';
}
$nav_html .= '</ul>';
$nav_html .= '<nav>';

echo $nav_html;
?>