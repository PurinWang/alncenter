@extends('_template_web._layouts.main')

<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link href="/web_assets/v1/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
    <style>
        <!--
        -->
    </style>
@endsection
<!-- ================== /page-css ================== -->

<!-- content -->
@section('content')
    <!--  -->
    <div id="content">
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-editbutton="false"
                         data-widget-fullscreenbutton="false">
                        <!-- widget div-->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i></span>
                            <h2>{{trans('_menu.'.implode( '.', $module ).'.title')}}</h2>
                            <a class="btn btn-default btn-sm pull-left btn-add" style="margin-left:10px"><i class="fa fa-create"></i>{{trans('web.add')}}</a>
                        </header>
                        <div style="padding-bottom:10px;">
                            <br>日期:
                            <input class="form-control" type="date" id="orderStartDate" style="display:inline-block; width: 150px; margin-bottom: 5px;" value="{{date('Y-m-01')}}">
                            &nbsp;~&nbsp;
                            <input class="form-control" type="date" id="orderEndDate" style="display:inline-block; width: 150px; margin-bottom: 5px;" value="{{date('Y-m-d')}}">
                            <a class="btn btn-primary btn-sm btn-search">搜尋</a>
                            {{--<a class="btn btn-primary btn-excel" style="margin:10px 10px"><i class="fa fa-create"></i>匯出</a>--}}
                            <br>
                        </div>
                        <div>
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" style="width:100%">
                                </table>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </article>
                <!-- WIDGET END -->
            </div>
            <!-- end row -->
        </section>
        <!-- end widget grid -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">新增</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">會員ID</span>
                                <input type="text" class="form-control iMemberId" placeholder="會員ID"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">取得方式</span>
                                <input type="text" class="form-control vType" placeholder="取得方式"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">Bonus</span>
                                <input type="number" class="form-control iBonus" placeholder="20"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">投資意願單號</span>
                                <input type="text" class="form-control vBonusNum" placeholder="AE1234567"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('web.cancel')}}</button>
                    <button type="button" class="btn btn-primary btn-doadd">{{trans('web.doadd')}}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- Modal -->
    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">編輯</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">會員ID</span>
                                <input type="text" class="form-control iMemberId" placeholder="會員ID"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">取得方式</span>
                                <input type="text" class="form-control vType" placeholder="取得方式"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">Bonus</span>
                                <input type="number" class="form-control iBonus" placeholder="20"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">投資意願單號</span>
                                <input type="text" class="form-control vBonusNum" placeholder="AE1234567"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('web.cancel')}}</button>
                    <button type="button" class="btn btn-primary btn-dosave">{{trans('web.dosave')}}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- Modal -->
    <div class="modal fade" id="view-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">會員資料</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">姓名</span>
                                <input type="text" class="form-control vUserName" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">Email</span>
                                <input type="text" class="form-control vUserEmail" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">國別</span>
                                <input type="text" class="form-control vCountry" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">護照號碼</span>
                                <input type="text" class="form-control vUserID" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">ERC20 Address</span>
                                <input type="text" class="form-control vERC" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">出生日期</span>
                                <input type="text" class="form-control iBirthday" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">稱呼</span>
                                <input type="text" class="form-control vUserTitle" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">聯絡地址</span>
                                <input type="text" class="form-control vAddress" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group input-group">
                                <span class="input-group-addon">聯絡電話</span>
                                <input type="text" class="form-control vContact" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group input-group vPassportF">
                                <span class="input-group-addon">圖片</span>
                                <a href="" title="Image" data-gallery="">
                                    <img src="" style="height:150px">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group input-group vPassportB">
                                <span class="input-group-addon">圖片</span>
                                <a href="" title="Image" data-gallery="">
                                    <img src="" style="height:150px">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group input-group vPassportT">
                                <span class="input-group-addon">圖片</span>
                                <a href="" title="Image" data-gallery="">
                                    <img src="" style="height:150px">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('web.close')}}</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <!-- blueimp gallery -->
    <script src="/web_assets/v1/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
    <script src="/web_assets/v3/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        var current_data = [];
        var ajax_source = "{{ url('web/'.implode( '/', $module ).'/getlist')}}";
        var ajax_Table = "{{ url('web/'.implode( '/', $module ).'/getlist')}}";
        var url_doadd = "{{ url('web/'.implode( '/', $module ).'/doadd')}}";
        var url_dosave = "{{ url('web/'.implode( '/', $module ).'/dosave')}}";
        var url_dodel = "{{ url('web/'.implode( '/', $module ).'/dodel')}}";
        $(document).ready(function () {
            /* BASIC ;*/
            var i = 0;
            var table = $('#dt_basic').dataTable({
                "serverSide": true,
                "stateSave": true,
                "scrollX": true,
                "scrollY": "60vh",
                "aoColumns": [
                    {"sTitle": "Id", "mData": "iId", "width": "5%", "sName": "iId"},
                    {
                        "sTitle": "狀態",
                        "mData": "iStatus",
                        "sName": "iStatus",
                        "bSearchable": false,
                        "bSortable": true,
                        "width": "5%",
                        "mRender": function (data, type, row) {
                            var btn = "無狀態";
                            switch (data) {
                                case 1:
                                    btn = '<button class="btn btn-xs btn-danger btn-status">已確認</button>';
                                    break;
                                default:
                                    btn = '<button class="btn btn-xs btn-primary btn-status">未確認</button>';
                                    break;
                            }
                            return btn;
                        }
                    },
                    {
                        "sTitle": "Action",
                        "width": "10%",
                        "bSortable": false,
                        "bSearchable": false,
                        "mRender": function (data, type, row) {
                            current_data[row.iId] = row;
                            var btn = "無功能";
                            btn = '<button class="btn btn-xs btn-default btn-view" title="檢視"><i class="fa fa-search" aria-hidden="true"></i></button>';
                            @if(session('member.iAcType') <= 2)
                                btn += '<button class="btn btn-xs btn-default btn-edit" title="修改"><i class="fa fa-pencil" aria-hidden="true"></i></button>';
                            btn += '<button class="pull-right btn btn-xs btn-default btn-del" title="刪除"><i class="fa fa-trash" aria-hidden="true"></i></button>';
                            @endif
                                return btn;
                        }
                    },
                    {"sTitle": "會員", "mData": "vUserName", "width": "10%", "sName": "sys_member_info.vUserName"},
                    {"sTitle": "Passport", "mData": "vUserID", "width": "10%", "sName": "sys_member_info.vUserID"},
                    {"sTitle": "Email", "mData": "vUserEmail", "width": "10%", "sName": "sys_member_info.vUserEmail"},
                    {"sTitle": "取得方式", "mData": "vType", "width": "10%", "sName": "vType"},
                    {
                        "sTitle": "Bonus",
                        "mData": "iBonus",
                        "width": "5%",
                        "sName": "iBonus"
                    },
                    {"sTitle": "意願單號", "mData": "vBonusNum", "width": "10%", "sName": "vBonusNum"},
                    {"sTitle": "時間", "mData": "iCreateTime", "width": "10%", "sName": "iCreateTime"},
                ],
                "sAjaxSource": ajax_source,
                "ajax": ajax_Table,
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                "autoWidth": true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
            });
            /* END BASIC */
            //
            $("#dt_basic").on('click', '.btn-status', function () {
                var id = $(this).closest('tr').attr('id');
                var data = {
                    "_token": "{{ csrf_token() }}"
                };
                data.iId = id;
                data.iStatus = "change";
                $.ajax({
                    url: url_dosave,
                    data: data,
                    type: "POST",
                    //async: false,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            toastr.success(rtndata.message, "{{trans('_web_alert.notice')}}")
                            setTimeout(function () {
                                table.api().ajax.reload(null, false);
                            }, 100);
                        } else {
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "error");
                        }
                    }
                });
            });
            //
            $("#dt_basic").on('click', '.btn-del', function () {
                var id = $(this).closest('tr').attr('id');
                var data = {
                    "_token": "{{ csrf_token() }}"
                };
                data.iId = id;
                swal({
                    title: "{{trans('_web_alert.del.title')}}",
                    text: "{{trans('_web_alert.del.note')}}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{trans('_web_alert.cancel')}}",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('_web_alert.ok')}}",
                    closeOnConfirm: false
                }, function () {
                    $.ajax({
                        url: url_dodel,
                        data: data,
                        type: "POST",
                        //async: false,
                        success: function (rtndata) {
                            if (rtndata.status) {
                                //toastr.success(rtndata.message,"{{trans('_web_alert.notice')}}")
                                swal("{{trans('_web_alert.notice')}}", rtndata.message, "success");
                                setTimeout(function () {
                                    table.api().ajax.reload(null, false);
                                }, 100);

                            } else {
                                swal("{{trans('_web_alert.notice')}}", rtndata.message, "error");
                            }
                        }
                    });
                });
            });
            //
            $("#dt_basic").on('click', '.btn-view', function () {
                var id = $(this).closest('tr').attr('id');
                modal = $("#view-modal");
                modal.data('id', id);
                modal.find('.vUserName').val(current_data[id].vUserName);
                modal.find('.vUserEmail').val(current_data[id].vUserEmail);
                modal.find('.vCountry').val(current_data[id].vCountry);
                modal.find('.vUserID').val(current_data[id].vUserID);
                modal.find('.vERC').val(current_data[id].vERC);
                modal.find('.iBirthday').val(current_data[id].iBirthday);
                modal.find('.vUserTitle').val(current_data[id].vUserTitle);
                modal.find('.vAddress').val(current_data[id].vAddress);
                modal.find('.vContact').val(current_data[id].vCountryCode + '-' + current_data[id].vContact);
                modal.find('.vPassportF a').attr('href', current_data[id].vPassportF);
                modal.find('.vPassportF img').attr('src', current_data[id].vPassportF);
                modal.find('.vPassportB a').attr('href', current_data[id].vPassportB);
                modal.find('.vPassportB img').attr('src', current_data[id].vPassportB);
                modal.find('.vPassportT a').attr('href', current_data[id].vPassportF);
                modal.find('.vPassportT img').attr('src', current_data[id].vPassportT);
                modal.modal();
            });
            //
//            $('#orderPayStatus').change(function () {
//                search();
//            });
//            $('#orderStatus').change(function () {
//                search();
//            });
            $('.btn-search').click(function () {
                search();
            });
            function search() {
                //var myParams = table.api().ajax.params();
                var search_data = "?";
                var orderStartDate = $("#orderStartDate").val();
                var orderEndDate = $("#orderEndDate").val();
                if (orderStartDate != "" && orderEndDate != "") {
                    search_data += "orderStartDate=" + orderStartDate + "&orderEndDate=" + orderEndDate;
                }
                table.api().ajax.url(ajax_Table + search_data).load();
            }

            //
            $("#dt_basic").on('click', '.btn-edit', function () {
                var id = $(this).closest('tr').attr('id');
                modal = $("#edit-modal");
                current_modal = modal;
                modal.data('id', id);
                modal.find(".iMemberId").val(current_data[id].iMemberId);
                modal.find(".vType").val(current_data[id].vType);
                modal.find(".iBonus").val(current_data[id].iBonus);
                modal.find(".vBonusNum").val(current_data[id].vBonusNum);
                modal.modal();
            });
            //
            $(".btn-dosave").click(function () {
                var data = {"_token": "{{ csrf_token() }}"};
                data.iId = modal.data('id');
                data.iMemberId = modal.find('.iMemberId').val();
                data.vType = modal.find('.vType').val();
                data.iBonus = modal.find('.iBonus').val();
                data.vBonusNum = modal.find(".vBonusNum").val();
                $.ajax({
                    url: url_dosave,
                    type: "POST",
                    data: data,
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal.modal('toggle');
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "success");
                            setTimeout(function () {
                                table.api().ajax.reload(null, false);
                            }, 1000)
                        } else {
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "error");
                        }
                    }
                });
            });
            //
            $(".btn-add").click(function () {
                var modal = $("#add-modal");
                current_modal = modal;
                modal.modal();
            });
            //
            $(".btn-doadd").click(function () {
                var modal = $("#add-modal");
                var data = {"_token": "{{ csrf_token() }}"};
                data.iMemberId = modal.find('.iMemberId').val();
                data.vType = modal.find('.vType').val();
                data.iBonus = modal.find('.iBonus').val();
                data.vBonusNum = modal.find(".vBonusNum").val();
                $.ajax({
                    url: url_doadd,
                    type: "POST",
                    data: data,
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal.modal('toggle');
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "success");
                            setTimeout(function () {
                                table.api().ajax.reload(null, false);
                            }, 1000)
                        } else {
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "error");
                        }
                    }
                });
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->
