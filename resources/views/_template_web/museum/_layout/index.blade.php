@extends('_template_web._layouts.main')

<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <style>
        <!--
        -->
    </style>
@endsection
<!-- ================== /page-css ================== -->

<!-- content -->
@section('content')
    <!--  -->
    <div id="content">
        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">
                <div class="widget-body">
                    <div class="row">
                        <article class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>{{trans('_menu.'.implode( '.', $module ).'.title')}}</h2>
                                </header>
                                <!-- widget div-->
                                <div>
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                    </div>
                                    <!-- end widget edit box -->
                                    <!-- widget content -->
                                    <div class="widget-body">
                                        <div class="row">
                                            <form id="wizard-1" novalidate="novalidate" class="form-horizontal">
                                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab1">
                                                            <br>
                                                            <div class="form-group">
                                                                <label class="col-sm-1 col-md-1 col-lg-1 control-label">語系</label>
                                                                <div class="col-sm-10 col-md-8 col-lg-10">
                                                                    <select class="form-control" id="vMuseumName">
                                                                        @foreach( config( '_config.lang' ) as $key => $item)
                                                                            <option value="{{$key}}" @if($info->vMuseumCode == $key) selected @endif>{{$item}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-1 col-md-1 col-lg-1 control-label">簡介</label>
                                                                <div class="col-sm-10 col-md-8 col-lg-10">
                                                                    <textarea class="form-control" id="vMuseumSummary" placeholder="簡介">{{$info->vMuseumSummary}}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-1 col-md-1 col-lg-1 control-label">{{trans('web.show')}}</label>
                                                                <div class="col-sm-10 col-md-8 col-lg-10">
                                                                    <div class="radio">
                                                                        <label> <input type="radio" class="radiobox" name="bShow" value="0"
                                                                                       @if($info->bShow == 0) checked @endif><span>{{trans('web.show_false')}}</span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="radio">
                                                                        <label> <input type="radio" class="radiobox" name="bShow" value="1"
                                                                                       @if($info->bShow == 1) checked @endif><span>{{trans('web.show_true')}}</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-1 col-md-1 col-lg-1 control-label">{{trans('web.open')}}</label>
                                                                <div class="col-sm-10 col-md-8 col-lg-10">
                                                                    <div class="radio">
                                                                        <label> <input type="radio" class="radiobox" name="bOpen" value="0"
                                                                                       @if($info->bOpen == 0) checked @endif><span>{{trans('web.open_false')}}</span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="radio">
                                                                        <label> <input type="radio" class="radiobox" name="bOpen" value="1"
                                                                                       @if($info->bOpen == 1) checked @endif><span>{{trans('web.open_true')}}</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-sm-1 col-md-1 col-lg-1 control-label"></label>
                                                                <div class="pull-right col-sm-2 col-md-2 col-lg-2">
                                                                    <button class="btn btn-primary btn-save" type="button">
                                                                        <i class="fa fa-save"></i> {{trans('web.save')}}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget div -->
                            </div>
                            <!-- end widget -->
                        </article>
                    </div>
                </div>
            </article>
            <!-- NEW WIDGET START -->

            <!-- WIDGET END -->
        </div>
        <!-- end row -->
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        var url_index = "{{ url('web/'.implode( '/', $module ))}}";
        var url_dosave = "{{ url('web/'.implode( '/', $module ).'/dosave')}}";
        $(document).ready(function () {
            //
            $(".btn-save").click(function () {
                var data = {"_token": "{{ csrf_token() }}"};
                data.vMuseumName = $("#vMuseumName").find(":selected").text();
                data.vMuseumCode = $("#vMuseumName").find(":selected").val();
                data.vMuseumSummary = $("#vMuseumSummary").val();
                data.bShow = $("input[name=bShow]:checked").val();
                data.bOpen = $("input[name=bOpen]:checked").val();
                $.ajax({
                    url: url_dosave,
                    type: "POST",
                    data: data,
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "success");
                            setTimeout(function () {
                                location.reload();
                            }, 1000)
                        } else {
                            swal("{{trans('_web_alert.notice')}}", rtndata.message, "error");
                        }
                    }
                });
            });
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->
