<?php

Route::group(
    [
        'prefix' => 'autorun',
        'namespace' => '_AutoRun'
    ], function() {
    Route::group(
        [
            'prefix' => 'exchange_rate',
            'namespace' => 'ExchangeRate'
        ], function() {
        Route::post( 'getExchangeRate', 'IndexController@getExchangeRate' );
    } );
} );

/**
 * **************************************
 * 支付功能
 * *************************************
 */
Route::group( [
    'prefix' => 'payservice',
    'namespace' => '_PayService',
], function() {

} );

/*
 *
 */
Route::get( '', '_Web\LoginController@index' );
Route::group(
    [
        'middleware' => 'CheckLang',
        'prefix' => 'web',
        'namespace' => '_Web'
    ], function() {
    //
    Route::get( 'login', 'LoginController@index' );
    Route::post( 'doLogin', 'LoginController@doLogin' );
    //
    Route::get( 'logout', 'LogoutController@index' );
    Route::post( 'doLogout', 'LogoutController@doLogout' );
    //
    Route::post( 'doSetLocale/{locale}', 'LocaleController@doSetLocale' );
    //
    Route::group(
        [
            'middleware' => 'CheckLogin'
        ], function() {
        Route::get( '', 'IndexController@index' );
        //
        Route::post( 'upload_image', 'UploadController@doUploadImage' );
        Route::post( 'upload_image_base64', 'UploadController@doUploadImageBase64' );
        /**********************************************************
         * Member
         *********************************************************/
        Route::group(
            [
                'prefix' => 'member',
                'namespace' => '_Member',
            ], function() {
            Route::get( 'userinfo', 'IndexController@index' );
            Route::post( 'userinfo/dosave', 'IndexController@doSave' );
            Route::post( 'userinfo/dosavepassword', 'IndexController@doSavePassword' );
        } );

        /***********************************************************
         * Admin
         ***********************************************************/
        Route::group(
            [
                'prefix' => 'admin',
                'namespace' => '_Admin',
                'middleware' => 'CheckAdmin'
            ], function() {
            //
            Route::group(
                [
                    'prefix' => 'member',
                    'namespace' => 'Member',
                ], function() {
                Route::group(
                    [
                        'prefix' => 'customer'
                    ], function() {
                    Route::get( '', 'CustomerController@index' );
                    Route::get( 'getlist', 'CustomerController@getList' );
                    Route::post( 'doadd', 'CustomerController@doAdd' );
                    Route::post( 'dosave', 'CustomerController@doSave' );
                    Route::get( 'access/{id}', 'CustomerController@access' );
                    Route::post( 'dosaveaccess', 'CustomerController@doSaveAccess' );
                    Route::post( 'doupgrade', 'CustomerController@doUpGrade' );
                } );
                Route::group(
                    [
                        'prefix' => 'employee'
                    ], function() {
                    Route::get( '', 'EmployeeController@index' );
                    Route::get( 'getlist', 'EmployeeController@getList' );
                    Route::post( 'doadd', 'EmployeeController@doAdd' );
                    Route::post( 'dosave', 'EmployeeController@doSave' );
                    Route::get( 'access/{id}', 'EmployeeController@access' );
                    Route::post( 'dosaveaccess', 'EmployeeController@doSaveAccess' );
                } );
            } );
            //
            Route::group(
                [
                    'prefix' => 'group',
                    'namespace' => 'Group',
                ], function() {
                Route::group(
                    [
                        'prefix' => 'customer'
                    ], function() {
                    Route::get( '', 'CustomerController@index' );
                    Route::get( 'getlist', 'CustomerController@getList' );
                    Route::get( 'getmember', 'CustomerController@getMember' );
                    Route::post( 'doadd', 'CustomerController@doAdd' );
                    Route::post( 'dosave', 'CustomerController@doSave' );
                    Route::post( 'dodel', 'CustomerController@doDel' );
                } );
                Route::group(
                    [
                        'prefix' => 'employee'
                    ], function() {
                    Route::get( '', 'EmployeeController@index' );
                    Route::get( 'getlist', 'EmployeeController@getList' );
                    Route::get( 'getmember', 'EmployeeController@getMember' );
                    Route::post( 'doadd', 'EmployeeController@doAdd' );
                    Route::post( 'dosave', 'EmployeeController@doSave' );
                    Route::post( 'dodel', 'EmployeeController@doDel' );
                } );
            } );
            //
            Route::group(
                [
                    'prefix' => 'exchange_rate',
                    'namespace' => 'ExchangeRate',
                ], function() {
                Route::group(
                    [
                        'prefix' => 'index'
                    ], function() {
                    Route::get( '', 'IndexController@index' );
                    Route::get( 'getlist', 'IndexController@getList' );
                    Route::post( 'dosave', 'IndexController@doSave' );
                } );
                Route::group(
                    [
                        'prefix' => 'log'
                    ], function() {
                    Route::get( '', 'LogController@index' );
                    Route::get( 'getlist', 'LogController@getList' );
                } );
            } );
            //
            Route::group(
                [
                    'prefix' => 'category',
                ], function() {
                Route::get( '', 'CategoryController@index' );
                Route::get( 'getlist', 'CategoryController@getList' );
                Route::post( 'dosave', 'CategoryController@doSave' );
                Route::post( 'doadd', 'CategoryController@doAdd' );
                Route::group(
                    [
                        'prefix' => 'sub_{id}'
                    ], function() {
                    Route::get( '', 'CategoryController@sub' );
                    Route::get( 'getlist', 'CategoryController@getListSub' );
                    Route::post( 'dosave', 'CategoryController@doSaveSub' );
                    Route::post( 'doadd', 'CategoryController@doAddSub' );
                } );
            } );
            //
            Route::group(
                [
                    'prefix' => 'config',
                ], function() {
                Route::get( '', 'ConfigController@index' );
                Route::get( 'getlist', 'ConfigController@getList' );
                Route::post( 'dosave', 'ConfigController@doSave' );
                Route::post( 'doadd', 'ConfigController@doAdd' );
            } );
        } );
        /*************************************************************
         * Museum
         *************************************************************/
        Route::group(
            [
                'prefix' => 'museum',
                'namespace' => 'Museum',
            ], function() {
            Route::group(
                [
                    'prefix' => 'a01',
                ], function() {
                Route::get( '', 'A01Controller@index' );
                Route::post( 'dosave', 'A01Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a02',
                ], function() {
                Route::get( '', 'A02Controller@index' );
                Route::post( 'dosave', 'A02Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a03',
                ], function() {
                Route::get( '', 'A03Controller@index' );
                Route::post( 'dosave', 'A03Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a04',
                ], function() {
                Route::get( '', 'A04Controller@index' );
                Route::post( 'dosave', 'A04Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a05',
                ], function() {
                Route::get( '', 'A05Controller@index' );
                Route::post( 'dosave', 'A05Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a06',
                ], function() {
                Route::get( '', 'A06Controller@index' );
                Route::post( 'dosave', 'A06Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a07',
                ], function() {
                Route::get( '', 'A07Controller@index' );
                Route::post( 'dosave', 'A07Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a08',
                ], function() {
                Route::get( '', 'A08Controller@index' );
                Route::post( 'dosave', 'A08Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a09',
                ], function() {
                Route::get( '', 'A09Controller@index' );
                Route::post( 'dosave', 'A09Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a10',
                ], function() {
                Route::get( '', 'A10Controller@index' );
                Route::post( 'dosave', 'A10Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a11',
                ], function() {
                Route::get( '', 'A11Controller@index' );
                Route::post( 'dosave', 'A11Controller@doSave' );
            } );
            Route::group(
                [
                    'prefix' => 'a12',
                ], function() {
                Route::get( '', 'A12Controller@index' );
                Route::post( 'dosave', 'A12Controller@doSave' );
            } );
        } );
        /*************************************
         * 首頁資訊管理
         *************************************/
        Route::group(
            [
                'prefix' => 'home',
                'namespace' => 'Home',
            ], function() {
            Route::group(
                [
                    'prefix' => 'white_paper',
                ], function() {
                Route::get( '', 'WhitePaperController@index' );
                Route::get( 'getlist', 'WhitePaperController@getList' );
                Route::post( 'dosave', 'WhitePaperController@doSave' );
                Route::post( 'doadd', 'WhitePaperController@doAdd' );
            } );
            Route::group(
                [
                    'prefix' => 'activity',
                ], function() {
                Route::get( '', 'ActivityController@index' );
                Route::get( 'getlist', 'ActivityController@getList' );
                Route::post( 'dosave', 'ActivityController@doSave' );
                Route::post( 'doadd', 'ActivityController@doAdd' );
                Route::post( 'dodel', 'ActivityController@doDel' );
            } );
            Route::group(
                [
                    'prefix' => 'milestone',
                ], function() {
                Route::get( '', 'MilestoneController@index' );
                Route::get( 'getlist', 'MilestoneController@getList' );
                Route::post( 'dosave', 'MilestoneController@doSave' );
                Route::post( 'doadd', 'MilestoneController@doAdd' );
            } );
            Route::group(
                [
                    'prefix' => 'light_paper',
                ], function() {
                Route::get( '', 'LightPaperController@index' );
                Route::get( 'getlist', 'LightPaperController@getList' );
                Route::post( 'dosave', 'LightPaperController@doSave' );
                Route::post( 'doadd', 'LightPaperController@doAdd' );
            } );
        } );
        /*-**********************************
        * ICO INVEST
        *************************************/
        Route::group(
            [
                'prefix' => 'invest',
                'namespace' => 'Invest',
            ], function() {
            Route::group(
                [
                    'prefix' => 'index',
                ], function() {
                Route::get( '', 'IndexController@index' );
                Route::get( 'getlist', 'IndexController@getList' );
                Route::post( 'dosave', 'IndexController@doSave' );
                Route::post( 'doadd', 'IndexController@doAdd' );
                Route::post( 'dodel', 'IndexController@doDel' )->middleware( 'CheckAdmin' );
            } );
            Route::group(
                [
                    'prefix' => 'index_1',
                ], function() {
                Route::get( '', 'IndexETHController@index' );
                Route::get( 'getlist', 'IndexETHController@getList' );
                Route::post( 'dosave', 'IndexETHController@doSave' );
                Route::post( 'doadd', 'IndexETHController@doAdd' );
                Route::post( 'dodel', 'IndexETHController@doDel' )->middleware( 'CheckAdmin' );
            } );
            Route::group(
                [
                    'prefix' => 'index_2',
                ], function() {
                Route::get( '', 'IndexBTCController@index' );
                Route::get( 'getlist', 'IndexBTCController@getList' );
                Route::post( 'dosave', 'IndexBTCController@doSave' );
                Route::post( 'doadd', 'IndexBTCController@doAdd' );
                Route::post( 'dodel', 'IndexBTCController@doDel' )->middleware( 'CheckAdmin' );
            } );
            Route::group(
                [
                    'prefix' => 'index_3',
                ], function() {
                Route::get( '', 'IndexUSDController@index' );
                Route::get( 'getlist', 'IndexUSDController@getList' );
                Route::post( 'dosave', 'IndexUSDController@doSave' );
                Route::post( 'doadd', 'IndexUSDController@doAdd' );
                Route::post( 'dodel', 'IndexUSDController@doDel' )->middleware( 'CheckAdmin' );
            } );
            //
            Route::group(
                [
                    'prefix' => 'bonus',
                ], function() {
                Route::get( '', 'BonusController@index' );
                Route::get( 'getlist', 'BonusController@getList' );
                Route::post( 'dosave', 'BonusController@doSave' )->middleware( 'CheckAdmin' );
                Route::post( 'doadd', 'BonusController@doAdd' )->middleware( 'CheckAdmin' );
                Route::post( 'dodel', 'BonusController@doDel' )->middleware( 'CheckAdmin' );
            } );
        } );
        /*-**********************************
       * COIN AIR DROPS
       *************************************/
        Route::group(
            [
                'prefix' => 'coin_airdrops',
                'namespace' => 'CoinAirdrops',
            ], function() {
            Route::group(
                [
                    'prefix' => 'index',
                ], function() {
                Route::get( '', 'IndexController@index' );
                Route::get( 'getlist', 'IndexController@getList' );
                Route::post( 'dosave', 'IndexController@doSave' )->middleware( 'CheckAdmin' );
                Route::post( 'doadd', 'IndexController@doAdd' )->middleware( 'CheckAdmin' );
                Route::post( 'dodel', 'IndexController@doDel' )->middleware( 'CheckAdmin' );
            } );
            //
            Route::group(
                [
                    'prefix' => 'user',
                ], function() {
                Route::get( '', 'UserController@index' );
                Route::get( 'getlist', 'UserController@getList' );
                Route::post( 'dosave', 'UserController@doSave' )->middleware( 'CheckAdmin' );
                Route::post( 'doadd', 'UserController@doAdd' )->middleware( 'CheckAdmin' );
                Route::post( 'dodel', 'UserController@doDel' )->middleware( 'CheckAdmin' );
            } );
        } );
        /*************************************
         * 訊息公告
         *************************************/
        Route::group(
            [
                'prefix' => 'news',
                'namespace' => 'News',
            ], function() {
            Route::group(
                [
                    'prefix' => 'index',
                ], function() {
                Route::get( '', 'IndexController@index' );
                Route::get( 'getlist', 'IndexController@getList' );
                Route::post( 'doadd', 'IndexController@doAdd' );
                Route::post( 'dosave', 'IndexController@doSave' );
                Route::post( 'dodel', 'IndexController@doDel' );
            } );
        } );
        /*************************************
         * Service
         *************************************/
        Route::group(
            [
                'prefix' => 'service',
                'namespace' => 'Service',
            ], function() {
            Route::group(
                [
                    'prefix' => 'message',
                ], function() {
                Route::get( '', 'MessageController@index' );
                Route::get( 'getlist', 'MessageController@getList' );
                Route::post( 'doreply', 'MessageController@doReply' );
                Route::post( 'dosave', 'MessageController@doSave' );
                Route::post( 'dodel', 'MessageController@doDel' );
                Route::post( 'doclosed', 'MessageController@doClosed' );
            } );
            Route::group(
                [
                    'prefix' => 'email',
                ], function() {
                Route::get( '', 'EmailController@index' );
                Route::get( 'getlist', 'EmailController@getList' );
                Route::post( 'dosave', 'EmailController@doSave' );
                Route::post( 'dodel', 'EmailController@doDel' );
            } );
        } );


        /*************************************
         * LOG
         *************************************/
        Route::group(
            [
                'prefix' => 'log',
                'namespace' => 'LOG',
            ], function() {

        } );
    } );
} );